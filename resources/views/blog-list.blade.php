@extends('layouts.frontend')
@section('title', 'Blog-List')

@section('content')

<hr class=" d-none d-sm-block" style="margin-top: -5px">
<section class="latest_news_2" id="latest_news_style_2" style="margin-top:-80px">
    <div class="container">
        <div class="row">
        @if($records)
        @foreach($records as $record)
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                 <div class="single_item">
                    <div class="item_wrapper">
                        <div class="blog-img">
                            <a href="{{route('blog-details',$record->slug)}}" title=""><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$record->filename}}" alt="" class="img-fluid"></a>
                        </div>
                        <h3><a href="{{route('blog-details',$record->slug)}}" title="">{{$record->title}}</a></h3> 
                    </div>
                    <div class="blog_title">
                        <ul class="post_bloger">
                            <li><i class="fas fa-user"></i>Admin</li> 
                            <li><i class="fas fa-clock"></i>{{ date('d F Y',strtotime($record->created_at))}}</li>
                        </ul>               
                    </div> 
                </div>
            </div>                       
            @endforeach
        </div>
        {{ $records->links() }}
        @else
        <div class="text-center">
            sorry no blog post
        </div>
        @endif
    </div>
</section><!-- End Our Blog -->

@endsection