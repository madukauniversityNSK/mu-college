
<!-- Footer -->  
<footer class="footer_2">
    <div class="container">
        <div class="footer_top"  style="margin-top: 20px">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer_single_col footer_intro">
                        <img src="{{asset('/assets/front/images/avatars/'.config('website.logo2'))}}" alt="" class="f_logo">
                    </div>
                </div>
                <div class="col-6 col-md-6 col-lg-2">
                    <div class="footer_single_col">
                        <h3>Useful Links</h3>
                        <ul class="location_info quick_inf0" style="margin-top:-15px">
                        <li><a href="{{route('index')}}">Home</a></li>
                            <li><a href="https://portal.mucollege.com.ng/get-invoice">Application</a></li>
                            <li><a href="https://portal.mucollege.com.ng/manual-application-form">Admission</a></li>
                            <li><a href="https://portal.mucollege.com.ng/careers">Careers</a></li>
                        </ul>                         
                    </div>
                </div>
                <div class="col-6 col-md-6 col-lg-2">
                    <div class="footer_single_col information" >
                        <h3>information</h3>
                        <ul class="quick_inf0" style="margin-top:-15px">
                            <li><a href="{{route('about')}}">About Us</a></li>
                            <li><a href="{{url('blog/list')}}">News</a></li>
                            <li><a href="{{route('staffs')}}">Staffs</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-6 col-md-6 col-lg-4">
                    <div class="footer_single_col contact" >
                        <h3>Contact Us</h3>
                        <p style="margin-top:-15px">{{ config('settings.school_address') }}</p>
                        <div class="contact_info">
                            <span class="text-warning">{{ config('settings.phone_number') }}</span> 
                            <div class="text-warning">{{ config('settings.email_address') }}</div>
                        </div>
                        <ul class="social_items d-flex list-unstyled">
                            <li><a href="{{ config('settings.facebook_url') }}"><i class="fab fa-facebook-f fb-icon"></i></a></li>
                            <li><a href="{{ config('settings.twitter_url') }}"><i class="fab fa-twitter twitt-icon"></i></a></li>
                            <li><a href="{{ config('settings.instagram_url') }}"><i class="fab fa-instagram ins-icon"></i></a></li>
                        </ul>
                    </div>
                </div>
                 <div class="col-12 col-md-12 col-lg-12">
                    <div class="copyright">
                    <a class="text-white">© {{ date('Y') }} {{ config('settings.school_name') }} All Rights Reserved.</a>
                    </div>
                 </div>
            </div>
        </div>
    </div>    
</footer><!-- End Footer -->


<section id="scroll-top" class="scroll-top">
    <h2 class="disabled">Scroll to top</h2>
    <div class="to-top pos-rtive">
        <a href="#"><i class = "flaticon-right-arrow"></i></a>
    </div>
</section>