<header class="">
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>    
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12 ">
                    <div class="info_wrapper">
                        <div class="contact_info">                   
        					<ul class="list-unstyled">
                                <li><i class="flaticon-phone-receiver"></i>{{ config('settings.phone_number') }}</li>
        						<li><i class="flaticon-mail-black-envelope-symbol"></i>{{ config('settings.email_address') }}</li>
        					</ul>                    
                        </div>
                        <div class="login_info">
                             <ul class="d-flex">
                                <li class="nav-item"><a href="{{url('login')}}" class="nav-link sign-in js-modal-show"><i class="flaticon-padlock"></i>Web Login</a></li>
                                <li class="nav-item"><a href="https://portal.mucollege.com.ng" class="nav-link join_now js-modal-show"><i class="flaticon-padlock"></i>Portal Login</a></li>
                            </ul>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="edu_nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-faded">
                <a class="navbar-brand" href="{{route('index')}}"><img style="height:60px" src="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" alt="logo"></a>
                <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav nav lavalamp ml-auto menu">
                        <li class="nav-item"><a href="{{route('index')}}" class="nav-link {{ Route::currentRouteName() == 'index' ? 'active' : '' }}">Home</a>
                        </li>
                        <li class="nav-item"><a href="#" class="nav-link">About The School</a>
                           <ul class="navbar-nav nav mx-auto">
                                <li class="nav-item"><a href="{{route('about')}}" class="nav-link">About School </a></li>
                                <li class="nav-item"><a href="{{route('staffs')}}" class="nav-link">Staffs</a></li>
                            </ul> 
                        </li>
                        
                        
                        <li class="nav-item"><a href="#" class="nav-link">Admissions</a>
                            <ul class="navbar-nav nav mx-auto">
                                <li class="nav-item"><a href="https://portal.mucollege.com.ng/application-form" class="nav-link">Fill Application Form Online</a></li>
                                <li class="nav-item"><a href="https://portal.mucollege.com.ng/manual-application-form" class="nav-link">Download Application Form</a></li>
                        
                                <li class="nav-item"><a href="https://portal.mucollege.com.ng/check-admission" class="nav-link">Check Admission Status</a></li> 
                            </ul> 
                        </li>
                        <li class="nav-item"><a href="#" class="nav-link">Careers</a>
                            <ul class="navbar-nav nav mx-auto">
                                <li class="nav-item"><a href="https://portal.mucollege.com.ng/careers" class="nav-link">Apply</a></li>
                            </ul>
                    
                    </li> 
                        
                        
                        <li class="nav-item"><a href="mailto:{{ config('settings.email_address') }}" class="nav-link">Contact</a></li>
                    </ul>
                </div>
            </nav><!-- END NAVBAR -->
        </div> 
    </div>
</header>