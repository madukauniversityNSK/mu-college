<!DOCTYPE html>
<html lang="en">
<head>

    <!-- META ============================================= -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="canonical" href="" />
    <!-- OG -->
    <meta name="robots" content="index, follow">
    @foreach($admin as $a)
    <meta name="keywords" content="{{ $a->school_name}}" />
    <meta name="author" content="{{ $a->school_name}}" />
    <meta name="description" content="pre-nursery,nursery,primary, Secondary Schools" />

    <meta property="og:url" content="" />
    <meta property="og:site_name" content="{{ $a->school_name}}"/>
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_us" />
    <meta property="og:title" content="{{ $a->school_name}}" />
    <meta property="og:description" content="{{ $a->school_name}}" />
    <meta property="og:image" content="preview.png')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="">
    <meta name="twitter:creator" content="{{ $a->school_name}}">
    <meta name="twitter:title" content="{{ $a->school_name}}">
    <meta name="twitter:description" content="{{ $a->school_name}}">

    <meta name="format-detection" content="telephone=no">

    <!-- FAVICONS ICON ============================================= -->

        <link rel="icon" href="{{config('website.icon')}}" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('/assets/front/images/avatars/'.config('website.icon'))}}" />

    <!-- PAGE TITLE HERE ============================================= -->
        <title>{{ $a->school_name}} </title>
    @endforeach

    <!-- MOBILE SPECIFIC ============================================= -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if lt IE 9]>
    <script src="{{asset('assets/front/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('assets/front/js/respond.min.js')}}"></script>
    <![endif]-->

    <!-- All PLUGINS CSS ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/assets.css')}}">

    <!-- TYPOGRAPHY ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/typography.css')}}">

    <!-- SHORTCODES ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/shortcodes/shortcodes.css')}}">

    <!-- STYLESHEETS ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style.css')}}">
    <link class="skin" rel="stylesheet" type="text/css" href="{{asset('assets/front/css/color/color-1.css')}}">

    <!-- REVOLUTION SLIDER CSS ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/vendors/revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/vendors/revolution/css/settings.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/vendors/revolution/css/navigation.css')}}">
    <link href="{{ asset('slick/slick/slick.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('slick/slick/slick-theme.css') }}" rel="stylesheet" type="text/css" />
    <!-- REVOLUTION SLIDER END -->
</head>
<body id="bg">
<div class="page-wraper">
    <div id="loading-icon-bx"></div>
    <!-- Header Top ==== -->
    <header class="">
        <div class="top-bar"  style="background-color: white;">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="topbar-left" ">
                        <ul>
                            <li><a href="#"  style="color: black;"><i class="fa fa-question-circle"></i>Ask a Question</a></li>
                            <li><a href="javascript:;"  style="color: black;><i class="fa fa-envelope-o"></i>{{config('website.AppEmail')}}</a></li>
                        </ul>
                    </div>
                    <div class="topbar-right">
                        <ul>
                            <li><a href="{{config('website.AppPortal')}}"><button class="btn-warning radius-md"><i class="fa fa-sign-in"></i>Portal Login</button></a></li>
                            <li><a href="{{url('login')}}"><button class="btn-success radius-md"><i class="fa fa-sign-in"></i>Web Login</button></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="sticky-header navbar-expand-lg">
            <div class="menu-bar clearfix">
                <div class="container clearfix">
                    <!-- Header Logo ==== -->
                    <div class="menu-logo">
                        <a href="/">
                            <img src="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" alt="">
                        </a>
                    </div>
                    <!-- Mobile Nav Button ==== -->
                    <button class="navbar-toggler collapsed menuicon justify-content-end" type="button" data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <!-- Author Nav ==== -->
                    <div class="secondary-menu">
                        <div class="secondary-inner">
                            <ul>
                                <li><a href="javascript:;" class="btn-link"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:;" class="btn-link"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="javascript:;" class="btn-link"><i class="fa fa-linkedin"></i></a></li>
                                <!-- Search Button ==== -->
                                <li class="search-btn"><button id="quik-search-btn" type="button" class="btn-link"><i class="fa fa-search"></i></button></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Search Box ==== -->
                    <div class="nav-search-bar">
                        <form action="#">
                            <input name="search" value="" type="text" class="form-control" placeholder="Type to search">
                            <span><i class="ti-search"></i></span>
                        </form>
                        <span id="search-remove"><i class="ti-close"></i></span>
                    </div>
                    <!-- Navigation Menu ==== -->
                    <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
                        <div class="menu-logo">
                            <a href="/"><img src="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" alt="" style="height:150px; width: 150px;"></a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="/">Home</a>
                            </li>
                            <li><a href="javascript:;">About The School <i class="fa fa-chevron-down"></i></a>
                                <ul class="sub-menu add-menu">
                                    <li class="add-menu-left">
                                        <ul>
                                            <li><a href="{{url('about')}}">About School </a></li>
                                            <li><a href="{{url('staffs')}}">Staffs</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="javascript:;">Applicants <i class="fa fa-chevron-down"></i></a>
                                <ul class="sub-menu add-menu">
                                    <li class="add-menu-left">
                                        <ul>
                                            <li><a href="{{config('website.AppPortal')}}/get-invoice">Generate Invoice For Application Fee </a></li>
                                            <li><a href="{{config('website.AppPortal')}}/application-form">Fill Application Form Online</a></li>
                                            <li><a href="{{config('website.AppPortal')}}/manual-application-form">Download Application Form</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#">Admissions <i class="fa fa-chevron-down"></i></a>
                                <ul class="sub-menu add-menu">
                                    <li class="add-menu-left">
                                        <ul>
                                            <li><a href="{{config('website.AppPortal')}}/admissionlist">View Admission List </a></li>
                                            <li><a href="{{config('website.AppPortal')}}/check-admission">Check Admission Status</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#visual">Gallery </a></li>
                            <li><a href="#">E-Library </a></li>
                            <li><a href="mailto:{{config('website.AppEmail')}}">Contact </a></li>
                        </ul>
                        <div class="nav-social-link">
                            <a href="javascript:;"><i class="fa fa-facebook"></i></a>
                            <a href="javascript:;"><i class="fa fa-google-plus"></i></a>
                            <a href="javascript:;"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                    <!-- Navigation Menu END ==== -->
                </div>
            </div>
        </div>
    </header>
    <!-- Header Top END ==== -->

    <!-- Content -->
    <div class="page-content bg-white">
        <!-- Main Slider -->
        <div class="rev-slider">
            <div id="rev_slider_486_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery36" data-source="gallery" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.3.0.2 fullwidth mode -->
                <div id="rev_slider_486_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
                    <ul>	<!-- SLIDE  -->
                        @php $i = 0+100;@endphp
                        @foreach($slider as $s)
                            @php $i++;@endphp
                        <li data-index="rs-{{$i}}"
                            data-transition="parallaxvertical"
                            data-slotamount="default"
                            data-hideafterloop="0"
                            data-hideslideonmobile="off"
                            data-easein="default"
                            data-easeout="default"
                            data-masterspeed="default"
                            data-thumb="error-404.html"
                            data-rotate="0"
                            data-fstransition="fade"
                            data-fsmasterspeed="1500"
                            data-fsslotamount="7"
                            data-saveperformance="off"
                            data-title="A STUDY ON HAPPINESS"
                            data-param1="" data-param2=""
                            data-param3="" data-param4=""
                            data-param5="" data-param6=""
                            data-param7="" data-param8=""
                            data-param9="" data-param10=""
                            data-description="Science says that Women are generally happier">
                            <!-- MAIN IMAGE -->
                            <img src="{{ asset('assets/uploaded') }}/{{ $s->filename }}" alt=""
                                 data-bgposition="center center"
                                 data-bgfit="cover"
                                 data-bgrepeat="no-repeat"
                                 data-bgparallax="10"
                                 class="rev-slidebg"
                                 data-no-retina
                                 style=""/>

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-shape tp-shapewrapper "
                                 id="slide-100-layer-1"
                                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                 data-width="full"
                                 data-height="full"
                                 data-whitespace="nowrap"
                                 data-type="shape"
                                 data-basealign="slide"
                                 data-responsive_offset="off"
                                 data-responsive="off"
                                 data-frames='[{"from":"opacity:0;","speed":1,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1,"to":"opacity:0;","ease":"Power4.easeOut"}]'
                                 data-textAlign="['left','left','left','left']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 5;background-color:rgba(2, 0, 11, 0.80);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption Newspaper-Title   tp-resizeme"
                                 id="slide-100-layer-2"
                                 data-x="['center','center','center','center']"
                                 data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']"
                                 data-voffset="['250','250','250','240']"
                                 data-fontsize="['50','50','50','30']"
                                 data-lineheight="['55','55','55','35']"
                                 data-width="full"
                                 data-height="none"
                                 data-whitespace="normal"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[10,10,10,10]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 6; font-family:rubik; font-weight:700; text-align:center; white-space: normal;">
                                {{$s->title}}
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption Newspaper-Subtitle   tp-resizeme"
                                 id="slide-100-layer-3"
                                 data-x="['center','center','center','center']"
                                 data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']"
                                 data-voffset="['210','210','210','210']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['left','left','left','left']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 7; white-space: nowrap; color:#fff; font-family:rubik; font-size:18px; font-weight:400;">

                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption Newspaper-Subtitle   tp-resizeme"
                                 id="slide-100-layer-4"
                                 data-x="['center','center','center','center']"
                                 data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']"
                                 data-voffset="['320','320','320','290']"
                                 data-width="['800','800','700','420']"
                                 data-height="['100','100','100','120']"
                                 data-whitespace="unset"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                                 style="z-index: 7; text-transform:capitalize; white-space: unset; color:#fff; font-family:rubik; font-size:18px; line-height:28px; font-weight:400;">

                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption Newspaper-Button rev-btn "
                                 id="slide-100-layer-5"
                                 data-x="['center','center','center','center']"
                                 data-hoffset="['90','80','75','90']"
                                 data-y="['top','top','top','top']"
                                 data-voffset="['400','400','400','420']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="button"
                                 data-responsive_offset="on"
                                 data-responsive="off"
                                 data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:1px 1px 1px 1px;"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[12,12,12,12]"
                                 data-paddingright="[30,35,35,15]"
                                 data-paddingbottom="[12,12,12,12]"
                                 data-paddingleft="[30,35,35,15]"
                                 style="z-index: 8; white-space: nowrap; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; background-color:var(--primary) !important; border:0; border-radius:30px; margin-right:5px;">READ MORE </div>
                            <div class="tp-caption Newspaper-Button rev-btn"
                                 id="slide-100-layer-6"
                                 data-x="['center','center','center','center']"
                                 data-hoffset="['-90','-80','-75','-90']"
                                 data-y="['top','top','top','top']"
                                 data-voffset="['400','400','400','420']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-type="button"
                                 data-responsive_offset="on"
                                 data-responsive="off"
                                 data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:1px 1px 1px 1px;"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[12,12,12,12]"
                                 data-paddingright="[30,35,35,15]"
                                 data-paddingbottom="[12,12,12,12]"
                                 data-paddingleft="[30,35,35,15]"
                                 style="z-index: 8; white-space: nowrap; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; border-radius:30px;">CONTACT US</div>
                        </li>
                        @endforeach

                    </ul>
                </div><!-- END REVOLUTION SLIDER -->
            </div>
        </div>
        <!-- Main Slider -->
        <div class="content-block">

            <!-- Our Services -->
            <div class="section-area content-inner service-info-bx">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="service-bx">
                                <div class="action-box">
                                    <img src="{{asset('assets/front/images/index/1.jpeg')}}" alt="" style="height: 200px; width: 100%;">
                                </div>
                                <div class="info-bx text-center" style="height: 180px; overflow: hidden;">
                                    <div class="feature-box-sm radius bg-white">
                                        <i class="fa fa-file-text-o text-primary"></i>
                                    </div>
                                    <h4><a href="#">Grooming Better Students</a></h4>
                                    <a href="#" class="btn radius-xl">View More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="service-bx">
                                <div class="action-box">
                                    <img src="{{asset('assets/front/images/index/2.jpg')}}" alt="" style="height: 200px; width: 100%;">
                                </div>
                                <div class="info-bx text-center" style="height: 180px; overflow: hidden;">
                                    <div class="feature-box-sm radius bg-white">
                                        <i class="fa fa-book text-primary"></i>
                                    </div>
                                    <h4><a href="#">Dedicated Staffs</a></h4>
                                    <a href="#" class="btn radius-xl">View More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="service-bx m-b0">
                                <div class="action-box">
                                    <img src="{{asset('assets/front/images/index/3.jpeg')}}" alt="" style="height: 200px; width: 100%;">
                                </div>
                                <div class="info-bx text-center" style="height: 180px; overflow: hidden;">
                                    <div class="feature-box-sm radius bg-white">
                                        <i class="fa fa-bank text-primary"></i>
                                    </div>
                                    <h4><a href="#">Good Learning Environment</a></h4>
                                    <a href="#" class="btn radius-xl">View More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Our Services END -->

            <!-- Popular Courses -->
            <div class="section-area section-sp2 popular-courses-bx" id="visual">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 heading-bx left">
                            <h2 class="title-head">TAKE A <span>VISUAL TOUR</span></h2>
                            <p>Go through our school gallery</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="courses-carousel owl-carousel owl-btn-1 col-12 p-lr0">
                            @foreach($gallery as $g)
                            <div class="item">
                                <div class="cours-bx">
                                    <div class="action-box">
                                        <img src="{{ asset('assets/uploaded') }}/{{ $g->filename }}" alt="">
                                        <a href="#" class="btn">View More</a>
                                    </div>
                                    <div class="info-bx text-center">
                                        <h5><a href="#">{{$g->title}}</a></h5>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- Popular Courses END -->

            <!-- Form -->
            <div class="section-area section-sp1 bg-fix online-cours" style="background-image:url(asset('assets/images/background/bg1.jpg')); background-color: #00356B">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center text-white">
                            <h2>WE PREPARE OUR STUDENTS FOR THE ADULT LIFE.</h2>
                            <h5>Owning their future..</h5>
                            <form class="cours-search">
                                        <button class="btn" type="button">Start Now</button>
                            </form>
                        </div>
                    </div>
                    <div class="mw800 m-auto">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="cours-search-bx m-b30">
                                    <div class="icon-box">
                                        <h3><i class="ti-user"></i><span class="counter">1</span>300</h3>
                                    </div>
                                    <span class="cours-search-text">1300 Students</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="cours-search-bx m-b30">
                                    <div class="icon-box">
                                        <h3><i class="ti-book"></i><span class="counter">3</span>0</h3>
                                    </div>
                                    <span class="cours-search-text">30 Teachers.</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="cours-search-bx m-b30">
                                    <div class="icon-box">
                                        <h3><i class="ti-layout-list-post"></i><span class="counter">7</span>00</h3>
                                    </div>
                                    <span class="cours-search-text">Applications Yearly.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form END -->
            <div class="section-area section-sp2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center heading-bx">
                            <h2 class="title-head m-b0">Upcoming <span>Events</span></h2>
                            <p class="m-b0">Upcoming Education Events To Feed Brain. </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="upcoming-event-carousel owl-carousel owl-btn-center-lr owl-btn-1 col-12 p-lr0  m-b30">
                            @foreach($events as $e)
                            <div class="item">
                                <div class="event-bx">
                                    <div class="action-box">
                                        <img src="{{ asset('assets/uploaded') }}/{{ $e->filename }}" alt="">
                                    </div>
                                    <div class="info-bx d-flex">
                                        <div>
                                            <div class="event-time">
                                                <div class="event-date">{{ date('d',strtotime($e->start_date))}}</div>
                                                <div class="event-month">{{ date('F',strtotime($e->start_date))}}</div>
                                            </div>
                                        </div>
                                        <div class="event-info">
                                            <h4 class="event-title"><a href="#">{{$e->event_name}}</a></h4>
                                            <ul class="media-post">
                                                <li><a href="#"><i class="fa fa-clock-o"></i> {{ date('g:i a',strtotime($e->start_date))}} - {{ date('g:i a',strtotime($e->end_date))}}</a></li>
                                                <li><a href="#"><i class="fa fa-map-marker"></i>{{$e->location}}</a></li>
                                            </ul>
                                            <p>{{$e->event_description}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn">View All Event</a>
                    </div>
                </div>
            </div>

            <!-- Recent News -->
            <div class="section-area section-sp2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 heading-bx left">
                            <h2 class="title-head">{{config('website.AppNameAlias')}} <span>News</span></h2>
                            <p></p>
                        </div>
                    </div>
                    <div class="recent-news-carousel owl-carousel owl-btn-1 col-12 p-lr0">
                        @foreach($posts as $p)
                        <div class="item">
                            <div class="recent-news">
                                <div class="action-box">
{{--                                    <img src="{{$p->attach ? url('uploads/'.$p->attach->document->path) : asset('pop/img/slider-bg-1.jpg')}}">--}}
                                    <img src="{{ asset('assets/uploaded') }}/{{ $p->filename }}">
                                </div>
                                <div class="info-bx">
                                    <ul class="media-post">
                                        <li><a href="#"><i class="fa fa-calendar"></i>{{ date('d F Y',strtotime($p->created_at))}}</a></li>
                                        <li><a href="#"><i class="fa fa-user"></i></a></li>
                                    </ul>
                                    <h5 class="post-title"><a href="#">
                                            {{$p->title}}
                                        </a></h5>
                                    <p>
                                        {!! substr(strip_tags($p->content),0,100) !!}{{ strlen(strip_tags($p->content)) > 100 ? "..." : "" }}
                                    </p>
                                    <div class="post-extra">
                                        <a href="{{route('single',$p->id)}}" class="btn-link">READ MORE</a>
{{--                                        @foreach(\App\Comment::where('post_id',$p->id)->count() as $count)--}}
{{--                                            <a href="#" class="comments-bx"><i class="fa fa-comments-o"></i>{{$count}} Comment</a>--}}
{{--                                        @endforeach--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- Recent News End -->

        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->
    <!-- Footer ==== -->
    <footer>
        <div class="" style="background-color: black;">
            <div class="pt-exebar">
                <div class="container">
                    <div class="d-flex align-items-stretch">
                        <div class="pt-logo mr-auto">
                            <a href="/"><img src="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" style="width: 100px; height: 100px;" alt=""/></a>
                        </div>
                        <div class="pt-social-link">
                            <ul class="list-inline m-a0">
                                <li><a href="#" class="btn-link"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="btn-link"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="btn-link"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="btn-link"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <div class="pt-btn-join">
                            <a href="#" class="btn ">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12 footer-col-4">
                        <div class="widget">
                            <h5 class="footer-title">Sign Up For A Newsletter</h5>
                            <p class="text-capitalize m-b20"></p>
                            <div class="subscribe-form m-b20">
                                <form class="subscription-form" action="http://educhamp.themetrades.com/demo/assets/script/mailchamp.php" method="post">
                                    <div class="ajax-message"></div>
                                    <div class="input-group">
                                        <input name="email" required="required"  class="form-control" placeholder="Your Email Address" type="email">
                                        <span class="input-group-btn">
											<button name="submit" value="Submit" type="submit" class="btn"><i class="fa fa-arrow-right"></i></button>
										</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                <div class="widget footer_widget">
                                    <h5 class="footer-title">School</h5>
                                    <ul>
                                        <li><a href="/">Home</a></li>
                                        <li><a href="{{url('about')}}">About</a></li>
                                        <li><a href="mailto:{{config('website.AppEmail')}}">Enquiries</a></li>
                                        <li><a href="mailto:{{config('website.AppEmail')}}">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                <div class="widget footer_widget">
                                    <h5 class="footer-title">Get In Touch</h5>
                                    <ul>
                                        <li><a href="{{config('website.AppPortal')}}/get-invoice">Applicants</a></li>
                                        <li><a href="{{config('website.AppPortal')}}/application-form">Admissions</a></li>
                                        <li><a href="#visual">Gallery</a></li>
                                        <li><a href="#event">Event</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                <div class="widget footer_widget">
                                    <h5 class="footer-title"></h5>
                                    <ul>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 col-md-5 col-sm-12 footer-col-4">
                        <div class="widget widget_gallery gallery-grid-4">
                            <h5 class="footer-title">Our Gallery</h5>
                            <ul class="magnific-image">
                                @foreach($photo as $ph)
                                <li><a href="{{ asset('assets/uploaded') }}/{{ $ph->filename }}" class="magnific-anchor"><img src="{{ asset('assets/uploaded') }}/{{ $ph->filename }}" alt=""></a></li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    @foreach($admin as $a)
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center"> © {{ date('Y') }} <span class="text-white">{{ $a->school_name}}</span>  All Rights Reserved.</div>
                    @endforeach
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer END ==== -->
    <button class="back-to-top fa fa-chevron-up" ></button>
</div>

<!-- External JavaScripts -->
<script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{asset('assets/front/vendors/magnific-popup/magnific-popup.js')}}"></script>
<script src="{{asset('assets/front/vendors/counter/waypoints-min.js')}}"></script>
<script src="{{asset('assets/front/vendors/counter/counterup.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/imagesloaded/imagesloaded.js')}}"></script>
<script src="{{asset('assets/front/vendors/masonry/masonry.js')}}"></script>
<script src="{{asset('assets/front/vendors/masonry/filter.js')}}"></script>
<script src="{{asset('assets/front/vendors/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('assets/front/js/functions.js')}}"></script>
<script src="{{asset('assets/front/js/contact.js')}}"></script>

<!-- Revolution JavaScripts Files -->
<script src="{{asset('assets/front/vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script src="{{ asset('slick/slick/slick.min.js') }}" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {
        var ttrevapi;
        var tpj =jQuery;
        if(tpj("#rev_slider_486_1").revolution == undefined){
            revslider_showDoubleJqueryError("#rev_slider_486_1");
        }else{
            ttrevapi = tpj("#rev_slider_486_1").show().revolution({
                sliderType:"standard",
                jsFileLocation:"//educhamp.themetrades.com/demo/xhtml/assets/vendors/revolution/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    mouseScrollReverse:"default",
                    onHoverStop:"on",
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,
                    arrows: {
                        style: "uranus",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        }
                    },

                },
                viewPort: {
                    enable:true,
                    outof:"pause",
                    visible_area:"80%",
                    presize:false
                },
                responsiveLevels:[1240,1024,778,480],
                visibilityLevels:[1240,1024,778,480],
                gridwidth:[1240,1024,778,480],
                gridheight:[768,600,600,600],
                lazyType:"none",
                parallax: {
                    type:"scroll",
                    origo:"enterpoint",
                    speed:400,
                    levels:[5,10,15,20,25,30,35,40,45,50,46,47,48,49,50,55],
                    type:"scroll",
                },
                shadow:0,
                spinner:"off",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
        }

        $('.slick').slick({

dots: true,
infinite: false,
speed: 300,
slidesToShow: 4,
slidesToScroll: 4,
arrows: true,
prevArrow: '<button type="button" class="slick-prev">Previous</button>',
nextArrow: '<button type="button" class="slick-next">Next</button>',
responsive: [
  {
    breakpoint: 1500,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      dots: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
});
    });
</script>
</body>
</html>

