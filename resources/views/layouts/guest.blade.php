@extends('layouts.app')

@section('content')
    <div class="container full-content-height margin-top-4em">
    @yield('inner_content')
</div>
@endsection

@section('footer')
@include('layouts.footer-min')
@endsection