<!DOCTYPE html>
<html lang="en">
<head>

    <!-- META ============================================= -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="canonical" href="" />
    <!-- OG -->
    <meta name="robots" content="index, follow">

    <meta name="keywords" content="{{config('website.AppMeta')}}" />
    <meta name="author" content="{{config('website.AppMeta')}}" />
    <meta name="description" content="pre-nursery,nursery,primary, Secondary Schools" />

    <meta property="og:url" content="" />
    <meta property="og:site_name" content="{{config('website.AppMeta')}}"/>
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_us" />
    <meta property="og:title" content="{{config('website.AppMeta')}}" />
    <meta property="og:description" content="{{config('website.AppMeta')}}" />
    <meta property="og:image" content="preview.png')}}"/>

    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="">
    <meta name="twitter:creator" content="{{config('website.AppMeta')}}">
    <meta name="twitter:title" content="{{config('website.AppMeta')}}">
    <meta name="twitter:description" content="{{config('website.AppMeta')}}">

    <meta name="format-detection" content="telephone=no">

    <!-- FAVICONS ICON ============================================= -->
    <link rel="icon" href="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" />

    <!-- PAGE TITLE HERE ============================================= -->
        <title> {{config('website.AppMeta')}} </title>

    <!-- MOBILE SPECIFIC ============================================= -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if lt IE 9]>
    <script src="{{asset('assets/front/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('assets/front/js/respond.min.js')}}"></script>
    <![endif]-->

    <!-- All PLUGINS CSS ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/assets.css')}}">
    <link href="{{asset('summernote/dist/summernote-lite.css')}}" rel="stylesheet">

    <!-- TYPOGRAPHY ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/typography.css')}}">

    <!-- SHORTCODES ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/shortcodes/shortcodes.css')}}">

    <!-- STYLESHEETS ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style.css')}}">
    <link class="skin" rel="stylesheet" type="text/css" href="{{asset('assets/front/css/color/color-1.css')}}">

    <!-- REVOLUTION SLIDER CSS ============================================= -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/vendors/revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/vendors/revolution/css/settings.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/vendors/revolution/css/navigation.css')}}">
    <!-- REVOLUTION SLIDER END -->
   
</head>
<body id="bg">
<div class="page-wraper">
    <div id="loading-icon-bx"></div>
    <!-- Header Top ==== -->
    <header class="header rs-nav">
        <div class="top-bar">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="topbar-left">
                        <ul>
                            <li><a href="#"><i class="fa fa-question-circle"></i>Ask a Question</a></li>
                            <li><a href="javascript:;"><i class="fa fa-envelope-o"></i>{{config('website.AppEmail')}}</a></li>
                        </ul>
                    </div>
                    <div class="topbar-right">
                        @if(Auth::check())
                            <ul>
                                <li><a href="{{url('home')}}"><i class="fa fa-home"></i>Dashboard</a></li>
                            </ul>
                        @else
                        <ul>
                            <li><a href="{{url('login')}}"><button class="btn-success"><i class="fa fa-sign-in"></i>Web Login</button></a></li>
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="sticky-header navbar-expand-lg">
            <div class="menu-bar clearfix">
                <div class="container clearfix">
                    <!-- Header Logo ==== -->
                    <div class="menu-logo">

                        <a href="/"><img src="{{config('website.logo')}}" alt=""></a>

                    </div>
                    <!-- Mobile Nav Button ==== -->
                    <button class="navbar-toggler collapsed menuicon justify-content-end" type="button" data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <!-- Author Nav ==== -->
                    <!-- Search Box ==== -->
                    <div class="nav-search-bar">
                        <form action="#">
                            <input name="search" value="" type="text" class="form-control" placeholder="Type to search">
                            <span><i class="ti-search"></i></span>
                        </form>
                        <span id="search-remove"><i class="ti-close"></i></span>
                    </div>
                    <!-- Navigation Menu ==== -->
                    <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
                        <div class="menu-logo">
                            <a href=""><img src="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" alt=""></a>
                        </div>
                        <ul class="nav navbar-nav">
                            @if(Auth::check())
                                <li><a href="{{url('home')}}">Dashboard<i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{url('all-about')}}">About Us Setup </a></li>
                                            </ul>
                                        </li>
                                    </ul></li>
                                <li><a href="javascript:;">Manage Blog<i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{url('all-posts')}}">All Blog Posts </a></li>
                                                <li><a href="{{url('create-post')}}">Create Blog Posts</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;">Manage Slider<i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{url('all-sliders')}}">All Slider Images </a></li>
                                                <li><a href="{{url('add-slider')}}">Create Slider Images</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{route('settings.index')}}"> Web Settings</a>
                                </li>
                                <li><a href="javascript:;">Manage Events<i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{url('all-event')}}">All Events Posts </a></li>
                                                <li><a href="{{url('add-event')}}">Create Events Posts</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;">Manage Gallery <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{url('all-gallery')}}">All Gallery </a></li>
                                                <li><a href="{{url('add-gallery')}}">Create Gallery</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;">Staff <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{url('all-staff')}}">All Staffs </a></li>
                                                <li><a href="{{url('add-staff')}}">Create Staffs</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{route('logout')}}" onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                                @else
                            <li class="active"><a href="/">Home</a>
                            </li>
                                <li><a href="javascript:;">About The School <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{url('about')}}">About School </a></li>
                                                <li><a href="{{url('staffs')}}">Staffs</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;">Applicants <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{config('website.AppPortal')}}/get-invoice">Generate Invoice For Application Fee </a></li>
                                                <li><a href="{{config('website.AppPortal')}}/application-form">Fill Application Form Online</a></li>
                                                <li><a href="{{config('website.AppPortal')}}/manual-application-form">Download Application Form</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#">Admissions <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu add-menu">
                                        <li class="add-menu-left">
                                            <ul>
                                                <li><a href="{{config('website.AppPortal')}}/admissionlist">View Admission List </a></li>
                                                <li><a href="{{config('website.AppPortal')}}/check-admission">Check Admission Status</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#visual">Gallery </a></li>
                                <li><a href="#event">Event</a></li>
                                <li><a href="#">E-Library </a></li>
                                <li><a href="mailto:{{config('website.AppEmail')}}">Contact </a></li>
                                @endif
                        </ul>
                        <div class="nav-social-link">
                            <a href="javascript:;"><i class="fa fa-facebook"></i></a>
                            <a href="javascript:;"><i class="fa fa-google-plus"></i></a>
                            <a href="javascript:;"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                    <!-- Navigation Menu END ==== -->
                </div>
            </div>
        </div>
    </header>
    <!-- Header Top END ==== -->
@yield('content')
<!-- Footer ==== -->
    <footer>
        @if(Auth::check())
            <div class="footer-bottom" style="position: fixed;bottom: 0;width: 100%;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center"> © 2020 <span class="text-white">{{config('website.AppName')}} Website Management.</span>  All Rights Reserved.</div>
                    </div>
                </div>
            </div>
        @else
        <div class="footer-top">
            <div class="pt-exebar">
                <div class="container">
                    <div class="d-flex align-items-stretch">
                        <div class="pt-logo mr-auto">
                            <a href="/"><img src="{{config('website.logo')}}" style="width: 100px; height: 100px;" alt=""/></a>
                        </div>
                        <div class="pt-social-link">
                            <ul class="list-inline m-a0">
                                <li><a href="#" class="btn-link"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="btn-link"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="btn-link"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="btn-link"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <div class="pt-btn-join">
                            <a href="#" class="btn ">Join Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12 footer-col-4">
                        <div class="widget">
                            <h5 class="footer-title">Sign Up For A Newsletter</h5>
                            <p class="text-capitalize m-b20"></p>
                            <div class="subscribe-form m-b20">
                                <form class="subscription-form" action="http://educhamp.themetrades.com/demo/assets/script/mailchamp.php" method="post">
                                    <div class="ajax-message"></div>
                                    <div class="input-group">
                                        <input name="email" required="required"  class="form-control" placeholder="Your Email Address" type="email">
                                        <span class="input-group-btn">
											<button name="submit" value="Submit" type="submit" class="btn"><i class="fa fa-arrow-right"></i></button>
										</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                <div class="widget footer_widget">
                                    <h5 class="footer-title">School</h5>
                                    <ul>
                                        <ul>
                                            <li><a href="/">Home</a></li>
                                            <li><a href="{{url('about')}}">About</a></li>
                                            <li><a href="mailto:{{config('website.AppEmail')}}">Enquiries</a></li>
                                            <li><a href="mailto:{{config('website.AppEmail')}}">Contact</a></li>
                                        </ul>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                <div class="widget footer_widget">
                                    <h5 class="footer-title">Get In Touch</h5>
                                    <ul>
                                        <li><a href="{{config('website.AppPortal')}}/get-invoice">Applicants</a></li>
                                        <li><a href="{{config('website.AppPortal')}}/application-form">Admissions</a></li>
                                        <li><a href="#visual">Gallery</a></li>
                                        <li><a href="#event">Event</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                <div class="widget footer_widget">
                                    <h5 class="footer-title"></h5>
                                    <ul>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 col-md-5 col-sm-12 footer-col-4">
                        <div class="widget widget_gallery gallery-grid-4">
                            <h5 class="footer-title"></h5>
                            <ul class="magnific-image">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center"> © 2020 <span class="text-white">{{config('website.AppName')}}</span>  All Rights Reserved.</div>
                </div>
            </div>
        </div>
            @endif
    </footer>
    <!-- Footer END ==== -->
    <button class="back-to-top fa fa-chevron-up" ></button>
</div>

<!-- External JavaScripts -->
<script src="{{asset('assets/front/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{asset('assets/front/vendors/magnific-popup/magnific-popup.js')}}"></script>
<script src="{{asset('assets/front/vendors/counter/waypoints-min.js')}}"></script>
<script src="{{asset('assets/front/vendors/counter/counterup.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/imagesloaded/imagesloaded.js')}}"></script>
<script src="{{asset('assets/front/vendors/masonry/masonry.js')}}"></script>
<script src="{{asset('assets/front/vendors/masonry/filter.js')}}"></script>
<script src="{{asset('assets/front/vendors/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('assets/front/js/functions.js')}}"></script>
<script src="{{asset('assets/front/js/contact.js')}}"></script>
<script src='assets/vendors/switcher/switcher.js'></script>
<!-- Revolution JavaScripts Files -->
<script src="{{asset('assets/front/vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('assets/front/vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script src="{{asset('summernote/dist/lang/summernote-es-ES.js')}}"></script>
<script>
   $('.summernote').summernote({

    });
</script>
<script>
    jQuery(document).ready(function() {
        var ttrevapi;
        var tpj =jQuery;
        if(tpj("#rev_slider_486_1").revolution == undefined){
            revslider_showDoubleJqueryError("#rev_slider_486_1");
        }else{
            ttrevapi = tpj("#rev_slider_486_1").show().revolution({
                sliderType:"standard",
                jsFileLocation:"//educhamp.themetrades.com/demo/xhtml/assets/vendors/revolution/js/",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    mouseScrollReverse:"default",
                    onHoverStop:"on",
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,
                    arrows: {
                        style: "uranus",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        }
                    },

                },
                viewPort: {
                    enable:true,
                    outof:"pause",
                    visible_area:"80%",
                    presize:false
                },
                responsiveLevels:[1240,1024,778,480],
                visibilityLevels:[1240,1024,778,480],
                gridwidth:[1240,1024,778,480],
                gridheight:[768,600,600,600],
                lazyType:"none",
                parallax: {
                    type:"scroll",
                    origo:"enterpoint",
                    speed:400,
                    levels:[5,10,15,20,25,30,35,40,45,50,46,47,48,49,50,55],
                    type:"scroll",
                },
                shadow:0,
                spinner:"off",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
        }
    });
</script>
</body>
</html>
