<!doctype html>
<html lang="en">
<head>
   
    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}" />
    
    <!--====== Title ======-->
    <title>@yield('title')</title>
    
    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{asset('storage/logo/favicon.png')}}" type="image/png">
    @include('backend.Partials.styles')
    @stack('css')
  
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            @include('backend.Partials.sidebar')
            @yield('content')
        </div>
    </div>
  
    @include('backend.Partials.scripts')
        @stack('scripts')
</body>
</html>

<body>