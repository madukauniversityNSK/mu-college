<div class="copyright">
    <div>
        <a href="https://creativecommons.org/licenses/by/4.0/">
            <!--src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"-->
            <img data-toggle="tooltip"
                 title="Every content on this page is OER compliant; use, share and improve upon."
                 class="image-responsive cc"
                 alt="Creative Commons License"
                 style="border-width:0"
                 src="{{asset('img/creativecommons.png')}}">
        </a>
    </div>
    <div>
        <span class="links">
            &copy; {{copyrightDate()}} 
            <a href="{{config('app.institute_url')}}"
               target="_blank">{{config('app.institute')}}</a>
        </span>
        <span class="links">
            Powered by 
            <a href="https://raadaa.com">Raadaa Partners Intl'</a>
        </span>
    </div>
</div>