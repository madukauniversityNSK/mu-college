<!doctype html>
<html lang="en">
<head>
   
    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}" />
    <meta name="robots" content="index, follow">

    <!-- FAVICONS ICON ============================================= -->

        <link rel="icon" href="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('/assets/front/images/avatars/'.config('website.logo'))}}" />

    
    <!--====== Title ======-->
    <title>@yield('title')</title>
    
    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{asset('storage/logo/favicon.png')}}" type="image/png">
    <style>
       img {
            display: block;
            max-width:1400px;
            max-height:559px;
            width: auto;
            height: auto;
        }
        .link-color{
            color: white;
        }
        .link-color:hover{
            color: red;
        }
    </style>
    @include('inc.style')
    @stack('css')
   

</head>

<body class="">
   @section('sidebar')
    @include('inc.header')
    @show
    <main >
        @yield('content')
        @include('inc.footer')
       
    </main>
    @include('inc.script')
     @stack('scripts')
  <script>

 </script>
</body>
</html>