<nav class="navbar navbar-default navbar-static-top navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{asset('img/logo.png')}}" style="height: 80px" alt=""/>
                <!--{{ config('app.name', 'Raadaa OER') }}-->
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <!--Create gap for logo-->
                <li>&nbsp;</li> 
                <li>&nbsp;</li> 
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('dashboard.plagiarism.index') }}" data-toggle="tooltip" data-placement="bottom" title="Plagiarism Checker">
                        Plagiarism Checker</a></li>
                <li><a href="{{ route('dashboard.impact.author') }}" data-toggle="tooltip" data-placement="bottom" title="Impact Measurement">
                        Impact Measurement</a></li>
                <li><a href="{{ route('dashboard.analytics.views') }}" data-toggle="tooltip" data-placement="bottom" title="View Analysis">
                        View Analysis</a></li>
               
                @yield('navlinks')
                @guest
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
                @else
                <li><a class="" href="{{ route('dashboard.index') }}">Feed</a></li>
                <li><a class="native-color lighten-1 white-text" href="{{ route('dashboard.upload') }}">Upload</a></li>

                @if(auth()->user()->admin)
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                        Management <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('admin.overview')}}">Overview</a></li>
                        <li><a href="{{route('admin.submissions.index')}}">Submissions</a></li>
                        <!--<li><a href="{{route('admin.analytics.views')}}">Analytics</a></li>-->
                    </ul>
                </li>
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                        {{ Auth::user()->first_name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li><a href=""></a></li>
                        <li><a href="{{ Auth::user()->url }}">Profile</a></li>
                        <li><a class="" href="{{ route('dashboard.library.index') }}">My Library</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
