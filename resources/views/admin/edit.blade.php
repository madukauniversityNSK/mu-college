@extends('layouts.admin')

@section('content')
    <a href="{{route('settings.index')}}" class="btn float-right">back</a>
    <h1 class="text-center my-4">
        Update logo and update school name
    </h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <form action="{{route('settings.update')}}" method="POST" enctype="multipart/form-data" class="my-4 text-center">
                   
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="">School Name</label>
                    <input type="text" class="form-control" value="{{ $admin->school_name }}" name="school_name">
                    </div>
                    <div class="form-group">
                        <label for="">Upload Logo</label>
                        <input type="file" value="{{ $admin->logo }}" class="form-control" name="logo">
                    </div>
                    <div class="form-group">
                    <input type="hidden" class="form-control" value="{{ $admin->id }}" name="id">
                    </div>
                    <button type="submit" class="btn">Upload</button>
                </form>
            </div>
        </div>
    </div>

@endsection