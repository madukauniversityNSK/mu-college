@extends('layouts.app')

@section('content')
    <!-- Main Quill library -->
    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <!-- Theme included stylesheets -->
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    {{--<!--[if lt IE 9]>--}}
    {{--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js') }}"></script>--}}
    {{--<script>window.jQuery || document.write('<script src="{{ asset('js/jquery-old.js') }}') }}"><\/script>')</script>--}}
    {{--<![endif]-->--}}

    
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <div class="page-cont" style="padding: 25px;">
        <div class="col-md-10 offset-1">
            @if(session()->has('message.level'))
                <div class="alert alert-{{ session('message.level') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong> {!! session('message.content') !!}</strong>
                </div>
            @endif
            <div class="item">
                <div class="service-bx">
                    <div class="info-bx d-flex">
                        <h2>Add Blog Post</h2>
                        <form enctype="multipart/form-data" action="{{ route('store-post') }}" method="POST" >
                            {{ csrf_field() }}
                            <div class="">
                                <label for="display_name">Category</label>
                                <select class="form-group " id="category_id" name="category_id" >
                                    <option value="1" >--Uncategorized --</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="">
                                <label for="title">Title</label><br/>
                                <input class="col-md-12" name="title" type="text" placeholder="post title">
                            </div>
                            <br/>
                            <div class="">
                                <label for="tags">Tags (optional)</label><br/>
                                <input class="col-md-12" name="tags" type="text" data-role="tagsinput" placeholder="comma separated. E.g James, Ujah, Paul">
                            </div>
                            <br/>
                            <div class="">
                                <label for="files">Select Feature Image</label>
                                <input type="file" class="col-md-8"  id="files" name="featured-image" accept="image/jpeg" onchange="readURL(this);" >
                            </div>
                            <br/>
                            <div class="">
            
                            <br/>
                             <div id="quill_editor"></div>
                             <input type="hidden" id="quill_html" name="content"></input>
                            <div class="row">
                                <button class="btn btn-primary" id="submit " type="submit" >Publish</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/><br/>
 <script>
    	var quill = new Quill('#quill_editor', {
        		theme: 'snow'
    	});
   quill.on('text-change', function(delta, oldDelta, source) {
        document.getElementById("quill_html").value = quill.root.innerHTML;
    });
</script>


</script>
@endsection
