@extends('layouts.app')

@section('content')
    <div class="page-cont" style="padding: 25px;">
        <div class="col-md-8 offset-2">
            @if(session()->has('message.level'))
                <div class="alert alert-{{ session('message.level') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <strong> {!! session('message.content') !!}</strong>
                </div>
            @endif
            <div class="item">
                <div class="service-bx">
                    <div class="info-bx d-flex">
                        <form method="post" action="{{route('update-post')}}" enctype="multipart/form-data">
                            @csrf
                            <h2>Edit Post</h2>
                            <input type="hidden" name="id" value="{{$post->id}}">
                            <div class="input-group">
                                <label>Post Title: </label>
                                <input class="col-lg-12" name="title" id="title" type="text" value="{{$post->title}}" required>
                            </div>
                            <br/>
                            <div class="input-group">
                                <label>Post Content: </label>
                                <textarea class="col-lg-12" name="contentz" id="title" type="text" required>
                                    {!! $post->content !!}
                                </textarea>
                            </div>
                            <br/>
                            <div class="input-group">
                                <label>Feature Image: </label>
                                <input class="col-lg-12" name="filename" id="filename" type="file">
                            </div>
                            <br/>
                            <div>
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/><br/><br/>
@endsection
