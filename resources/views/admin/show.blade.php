@extends('layouts.admin')

@section('content')
    <div class="my-4 text-center">
        <div class="">
            <a href="{{route('settings.edit', $admin->id)}}" class="btn-success px-5 py-3 my-3 float-left">Edit</a>
            <form action="" method="POST">

            </form>
        </div>
       <div class="">
            <h2>School Name: {{ $admin->school_name }}</h2>
            <img src="{{asset('/Storage/logo/'.$admin->logo)}}" class="img-thumbnail">
       </div>
       

    </div>
    
@endsection