@extends('layouts.app')

@section('content')

    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <div style="min-height: 50px;min-width: 100%; background-color: #4e342e; padding: 5px; margin: 5px;">
                    <p style="min-width: 100%;"><span style="font-size: 20px;  color: #fff; ">Create Menu Type</span></p>
                </div>

                <form enctype="multipart/form-data" action="{{ route('store-menu-type') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="display_name">Menu Types</label>

                                <ul  id="menu_type" name="menu_type" >

                                    @foreach($menu_types as $menu_type)
                                        <li >{{$menu_type->name}}</li>
                                    @endforeach

                                </ul>
                            </div>
                            <div class="form-group">
                                <label for="title">Menu Type</label>
                                <input class="form-control" name="name" type="text" placeholder="Primary">
                            </div>


                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-primary"  type="submit">Create </button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <div style="min-height: 50px;min-width: 100%; background-color: #4e342e; padding: 5px; margin: 5px;">
                    <p style="min-width: 100%;"><span style="font-size: 20px;  color: #fff; ">Create Menu Location</span></p>
                </div>
                <form enctype="multipart/form-data" action="{{ route('store-menu-location') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-xs-8">
                            <div class="form-group">
                                <label for="display_name">Menu Locations</label>

                                <ul  id="menu_type" name="menu_type" >

                                    @foreach($menu_locations as $menu_location)
                                        <li >{{$menu_location->name}}</li>
                                    @endforeach

                                </ul>
                            </div>
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input class="form-control" name="name" type="text" placeholder="Menu Location">
                            </div>


                        </div>
                    </div>

                    <div class="row">
                        <button class="btn btn-primary"  type="submit">Create</button>
                    </div>
                </form>
            </div>
        </div>
            <div class="row">
                <div class="col-md-12">
                    <form enctype="multipart/form-data" action="{{ route('store-menu') }}" method="POST">
                        <div style="min-height: 50px;min-width: 100%; background-color: #4e342e; padding: 5px; margin: 5px;">
                            <p style="min-width: 100%;"><span style="font-size: 20px;  color: #fff; ">Create Menu </span></p>
                        </div>
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-xs-8">

                                <div class="form-group">
                                    <label for="display_name">Menu Types</label>

                                    <select class="form-control " id="type_id" name="type_id" >

                                        @foreach($menu_types as $menu_type)
                                            <option value="{{$menu_type->id}}">{{$menu_type->name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="title">Menu</label>
                                    <input class="form-control" name="display" type="text" placeholder="Menu display" required>
                                </div>
                                <div class="form-group">
                                    <label for="title">Url</label>
                                    <input class="form-control" name="menu_url" type="text" placeholder="e.g https://google.com" required>
                                </div>
                                <div class="form-group">
                                    <label for="parent_id"> Parent Menu Menu (optional)</label>

                                    <select class="form-control " id="parent_id" name="parent_id" >

                                        @foreach($menus as $menu)
                                            <option value="{{$menu->id}}">{{$menu->display}}</option>
                                        @endforeach

                                    </select>
                                </div>



                            </div>
                        </div>

                        <div class="row">
                            <button class="btn btn-primary"  type="submit">Publish</button>
                        </div>
                    </form>
                </div>
            </div>



    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.featured-image')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: '#toolbar-container',
            },
            placeholder: 'Compose  post',
            theme: 'snow'
        });



        var form = document.querySelector('form');
        form.onsubmit = function() {
            var myEditor = document.querySelector('#editor-container');
            //var html =
            // Populate hidden form on submit
            var content = document.querySelector('input[name=content]');
            content.value = myEditor.children[0].innerHTML; //JSON.stringify(quill.setContents(content.value));
            return true;

        };







    </script>
@endsection
