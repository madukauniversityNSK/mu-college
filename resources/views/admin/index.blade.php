@extends('layouts.admin')

@section('content')
    @include('inc.messages')
    <div class="my-4">
        <a href="{{route('settings.create')}}" class="btn float-right">Upload a logo and school name</a>

        <div class="text-center">
            @if(count($admin)>0)
                @foreach($admin as $a)
                    <h2>School Name: {{ $a->school_name }}</h2>
                    <div class="mb-2">
                        <img src="{{asset('assets/uploaded/'.$a->logo)}}" class="thumbnail">
                    </div>
                    <a href="{{route('settings.show', $a->id)}}" class="btn-info px-5 py-2"> View this docs</a>
                @endforeach
            @endif
        </div>
    </div>
    
@endsection