@extends('layouts.admin')

@section('content')
    @include('inc.messages')
    <h1 class="text-center text-uppercase my-4">
        Add logo and school name
    </h1>
    <div class="container">
        <a href="{{route('settings.index')}}" class="btn float-right">back</a>
        <div class="row">
            <div class="col-md-6">
            <form action="{{route('settings.create')}}" method="POST" enctype="multipart/form-data" class="my-4 text-center">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="">School Name</label>
                    <input type="text" class="form-control" name="school_name">
                </div>
                <div class="form-group">
                    <label for="">Upload Logo</label>
                    <input type="file" class="form-control" name="logo">
                </div>
                <button type="submit" class="btn">Upload</button>
            </form>
            </div>
        </div>
    </div>
@endsection