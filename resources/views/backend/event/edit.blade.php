@extends('layouts.backend')
@section('title', 'Post Edit')

@section('content')
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Post Edit</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                        <form action="{{route('update-event')}}" method='POST' class="py-4" enctype="multipart/form-data">  
                                         @csrf
    
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Name</label>  
                                                <input type="text" class=" form-control form-control @error('event_name') is-invalid @enderror" id="exampleFormControlInput1"  name="event_name" value="{{old('event_name', $record->event_name)}}"  autofocus required>  
                                                @error('event_name')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Start Date</label>  
                                                <input type="datetime-local" class=" form-control form-control @error('start_date') is-invalid @enderror" id="exampleFormControlInput1"  name="start_date" value="{{old('start_date', $record->start_date)}}" required>  
                                                @error('start_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">End Date</label>  
                                                <input type="datetime-local" class=" form-control form-control @error('end_date') is-invalid @enderror" id="exampleFormControlInput1"  name="end_date" value="{{old('end_date', $record->end_date)}}" required >  
                                                @error('end_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset>
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Location</label>  
                                                <input type="text" class=" form-control form-control @error('location') is-invalid @enderror" id="exampleFormControlInput1"  name="location" value="{{old('location', $record->location)}}" required>  
                                                @error('location')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                                <input type="file" class=" form-control form-control @error('image') is-invalid @enderror" id="exampleFormControlInput1"  name="image" value="" >  
                                                @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset>
                                            <input type="hidden" class=" form-control form-control @error('id') is-invalid @enderror" id="exampleFormControlInput1"  name="id" value="{{old('id', $record->id)}}">  
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Description</label>  
                                                <textarea name="event_description"  class="form-control form-control @error('event_description') is-invalid @enderror"  >{{ old('event_description', $record->event_description) }}</textarea>
                                                    @error('event_description')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset>
                                            <div class="">
                                                <button class="btn btn-lg btn-primary float-right" type="submit"  >submit</button>
                                            </div>
    
                               </form>
                        </div>
                        <div class="col-sm-6">
                            <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$record->filename}}"  class="img-fluid" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script src="{{asset('summernote/dist/summernote-lite.js')}}"></script>
    <script src="{{asset('summernote/dist/lang/summernote-es-ES.js')}}"></script>
    <script>
    $('.summernote').summernote({

    });
    </script>
    
    @endpush
     
    @push('css')
      
    
    <link href="{{asset('summernote/dist/summernote-lite.css')}}" rel="stylesheet"></link>
 
    @endpush
    @endsection