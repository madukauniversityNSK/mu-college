@extends('layouts.backend')
@section('title', 'Event')

@section('content')
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left  text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Manage Event</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_tile">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Manage Events</a>
                        </li>
                        <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Create Event</a>
                        </li>
                    </ul>
                    <div class="tab-content py-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div>
                             @if (session()->has('message'))
                               <div class="alert alert-success">
                                  {{ session('message') }}
                                </div>
                              @endif
                            </div>
                            <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                   
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card-box table-responsive">
                                                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                               <th>S/N</th>
                                                                <th>Event Name</th>
                                                                <th>Event Description</th>
                                                                <th>Event Start</th>
                                                                <th>Event End</th>
                                                                <th>Event Location</th>
                                                                <th>Image</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                          @foreach($record as $key => $s)
                                                            <tr>
                                                                <td>{{$key + 1}}</td>
                                                                <td>{{$s->event_name}}</td>
                                                                <td>{{$s->event_description}}</td>
                                                                <td>{{date('d-F-Y',strtotime($s->start_date))}}</td>
                                                                <td>{{date('d-F-Y',strtotime($s->end_date))}}</td>
                                                                <td>{{$s->location}}</td>
                                                                <td><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$s->filename}}" height="70"></td>
                                                                <td>
                                                                    <div class="dropdown">
                                                                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fa fa-bars text-success"></i> 
                                                                        </a>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                                            <a class="dropdown-item" href="{{route('edit-event', $s->id)}}"><i class="fa fa-pencil"></i>  Edit</a>
                                                                            <a class="dropdown-item" href="{{route('delete-event', $s->id)}}"><i class="fa fa-remove"></i>  Delete</a>
                                                                        </div>
                                                                    </div>
                                                                </td> 
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="x_content">
                                <form action="{{route('save-event')}}" method='POST' class="py-4" enctype="multipart/form-data">  
                                         @csrf
                                    <div class="row">
                                         <div class="col-sm-6">
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Name</label>  
                                                <input type="text" class=" form-control form-control @error('event_name') is-invalid @enderror" id="exampleFormControlInput1"  name="event_name" value="{{old('event_name')}}"  autofocus required>  
                                                @error('event_name')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Start Date</label>  
                                                <input type="datetime-local" class=" form-control form-control @error('start_date') is-invalid @enderror" id="exampleFormControlInput1"  name="start_date" value="{{old('start_date')}}" required>  
                                                @error('start_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">End Date</label>  
                                                <input type="datetime-local" class=" form-control form-control @error('end_date') is-invalid @enderror" id="exampleFormControlInput1"  name="end_date" value="{{old('end_date')}}" required >  
                                                @error('end_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset>
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Location</label>  
                                                <input type="text" class=" form-control form-control @error('location') is-invalid @enderror" id="exampleFormControlInput1"  name="location" value="{{old('location')}}" required>  
                                                @error('location')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                                <input type="file" class=" form-control form-control @error('image') is-invalid @enderror" id="exampleFormControlInput1"  name="image" value="{{old('image')}}" required>  
                                                @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset>
                                           
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Description</label>  
                                                <textarea name="event_description"  class="form-control form-control @error('event_description') is-invalid @enderror"  >{{ old('event_description') }}</textarea>
                                                    @error('event_description')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset>
                                            <div class="">
                                                <button class="btn btn-lg btn-primary float-right" type="submit"  >submit</button>
                                            </div>
                                       </div>
                                    </div>
                               </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 @endsection