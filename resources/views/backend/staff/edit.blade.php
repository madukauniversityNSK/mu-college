@extends('layouts.backend')
@section('title', 'Staff Edit')

@section('content')
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Staff Edit</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <form action="{{route('update-staff')}}" method='POST' class="py-4" enctype="multipart/form-data">  
                                 @csrf
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Name</label>  
                                    <input type="text" class=" form-control form-control @error('name') is-invalid @enderror" id="exampleFormControlInput1"  name="name" value="{{old('name', $record->name )}}"  autofocus>  
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset> 
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">description</label>  
                                    <input name="description"  class=" form-control form-control @error('description') is-invalid @enderror" value="{{ old('description', $record->description) }}" required>
                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                    <input type="file" class=" form-control form-control @error('image') is-invalid @enderror" id="exampleFormControlInput1"  name="image" value=""   autofocus>  
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <input type="hidden"  name="id" value="{{old('id', $record->id)}}" autofocus>
                                <div class="">
                                    <button class="btn btn-lg btn-primary float-right" type="submit"  >Update</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6">
                            <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$record->filename}}"  class="img-fluid" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection