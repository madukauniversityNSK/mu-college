@extends('layouts.backend')
@section('title', 'Post Edit')

@section('content')
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Post Edit</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <form action="{{route('update-post')}}" method='POST' class="py-4" enctype="multipart/form-data">  
                                 @csrf
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Title</label>  
                                    <input type="text" class=" form-control form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1"  name="title" value="{{old('title', $record->title )}}"  required>  
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset> 
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                    <input type="file" class=" form-control form-control @error('image') is-invalid @enderror" id="exampleFormControlInput1"  name="image" value="{{old('image')}}" >  
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Content</label>  
                                    <textarea name="content"  class=" summernote form-control form-control @error('content') is-invalid @enderror" id="summernote"  required>{{ old('content', $record->content) }}</textarea>
                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                                <input type="hidden"  name="id" value="{{old('id', $record->id)}}" autofocus>
                                <div class="">
                                    <button class="btn btn-lg btn-primary float-right" type="submit"  >Update</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6">
                            <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$record->filename}}"  class="img-fluid" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script src="{{asset('summernote/dist/summernote-lite.js')}}"></script>
    <script src="{{asset('summernote/dist/lang/summernote-es-ES.js')}}"></script>
    <script>
    $('.summernote').summernote({

    });
    </script>
    
    @endpush
     
    @push('css')
      
    
    <link href="{{asset('summernote/dist/summernote-lite.css')}}" rel="stylesheet"></link>
 
    @endpush
    @endsection