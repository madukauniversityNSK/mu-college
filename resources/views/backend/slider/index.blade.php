@extends('layouts.backend')
@section('title', 'Slider')

@section('content')
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left  text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Manage Sliders</h>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_tile">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Manage Sliders</a>
                        </li>
                        <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Create Slider</a>
                        </li>
                    </ul>
                    <div class="tab-content py-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div>
                             @if (session()->has('message'))
                               <div class="alert alert-success">
                                  {{ session('message') }}
                                </div>
                              @endif
                            </div>
                            <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                   
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card-box table-responsive">
                                                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Title</th>
                                                                <th>image</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        @foreach($record as $records)
                                                            <tr>
                                                               
                                                                <td >{{ $records->title }}</td>
                                                                <td class="text-center"><img height="100" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$records->filename}}" class="" alt="slider"></td>
                                                                <td>
                                                                    <div class="dropdown">
                                                                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fa fa-bars text-success"></i> 
                                                                        </a>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                                            <a class="dropdown-item" href="{{route('edit-slider', $records->id)}}"><i class="fa fa-pencil"></i>  Edit</a>
                                                                            <a class="dropdown-item" href="{{route('delete-slider', $records->id)}}"><i class="fa fa-remove"></i>  Delete</a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="x_content">
                                <form action="{{route('save-slider')}}" method='POST' class="py-4" enctype="multipart/form-data">  
                                         @csrf
                                    <div class="row">
                                         <div class="col-sm-6">
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Title</label>  
                                                <input type="text" class=" form-control form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1"  name="title" value="{{old('title')}}"  autofocus required>  
                                                @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                             </fieldset>
                                             <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                                <input type="file" class=" form-control form-control @error('image') is-invalid @enderror" id="exampleFormControlInput1"  name="image" value=""  required>  
                                                @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </fieldset>  
                                            <div class="">
                                                <button class="btn btn-lg btn-primary float-right" type="submit"  >save</button>
                                            </div>
                                       </div>
                                    </div>
                               </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection