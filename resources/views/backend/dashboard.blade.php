@extends('layouts.backend')
@section('title', 'Dashbord')

@section('content')
    <div class="right_col" role="main">
    <div class="row">
           <div class="col-sm-6 col-xl-3" style="margin-bottom: 10px">
               <div class="card card-body bg-primary text-white">
                   <div class="media">
                       <div class="text-wh">
                           <h3 class="mb-0">{{$posts}}</h3>
                           <span class="text-uppercase font-size-xs font-weight-bold">Total Posts</span>
                       </div>

                       <div class="ml-3 align-self-center">
                       <i class="fa fa-newspaper-o fa-4x"></i>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-xl-3" style="margin-bottom: 10px">
               <div class="card card-body bg-secondary text-white">
                   <div class="media">
                       <div class="media-body">
                           <h3 class="mb-0">{{$gallery}}</h3>
                           <span class="font-size-xs">Total Galleries</span>
                       </div>

                       <div class="ml-3 align-self-center">
                          <i class="fa fa-file-image-o  fa-4x"></i>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-xl-3" style="margin-bottom: 10px">
               <div class="card card-body bg-info text-white">
                   <div class="media">
                       <div class="mr-3 align-self-center">
                          <i class="fa fa-sliders  fa-4x"></i>
                       </div>

                       <div class="media-body text-right" style="margin-bottom: 10px">
                           <h3 class="mb-0">{{$slider}}</h3>
                           <span class="font-size-xs">Total Slider</span>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-xl-3" style="margin-bottom: 10px">
               <div class="card card-body bg-success text-white">
                   <div class="media">
                       <div class="mr-3 align-self-center">
                          <i class="fa fa-map-marker fa-4x"></i>
                       </div>

                       <div class="media-body text-right">
                           <h3 class="mb-0">{{$events}}</h3>
                           <span class="text-uppercase font-size-xs">Total Events</span>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-sm-6 col-xl-3" style="margin-bottom: 10px">
               <div class="card card-body bg-warning text-white">
                   <div class="media">
                       <div class="mr-3 align-self-center">
                          <i class="fa fa-user fa-4x"></i>
                       </div>

                       <div class="media-body text-right">
                           <h3 class="mb-0">{{$users}}</h3>
                           <span class="text-uppercase font-size-xs">Total Users</span>
                       </div>
                   </div>
               </div>
           </div>
           <div class="col-sm-6 col-xl-3" style="margin-bottom: 10px">
               <div class="card card-body bg-success text-white">
                   <div class="media">
                       <div class="mr-3 align-self-center">
                          <i class="fa fa-users fa-4x"></i>
                       </div>

                       <div class="media-body text-right">
                           <h3 class="mb-0">{{$staffs}}</h3>
                           <span class="text-uppercase font-size-xs">Total Staffs</span>
                       </div>
                   </div>
               </div>
            </div>
       </div>  
    </div>
</x-backend>

@endsection