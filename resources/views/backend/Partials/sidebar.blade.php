<div class="col-md-3 left_col ">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{route('home')}}" class="site_title"> <img height="50" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{config('settings.logo')}}"></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
              </div>
              <div class="profile_info">
                <h2></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="{{route('home')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                  <li class=""><a href="{{route('all-staff')}}"><i class="fa fa-users "></i>Staffs</a></li>
                  <li><a href="{{route('all-event')}}"><i class="fa fa-map-marker"></i>Event</a></li>
                  <li><a href="{{route('all-posts')}}"><i class="fa fa-newspaper-o"></i>Posts</a></li>
                  <li><a href="{{route('all-sliders')}}"><i class="fa fa-sliders"></i>Sliders</a></li>
                  <li><a href="{{route('all-gallery')}}"><i class="fa fa-file-image-o"></i>Gallery</a></li>
                  @if(auth()->user()->user_type == "super_admin")
                  <li class=""><a href="{{route('user.index')}}"><i class="fa fa-users "></i>Admin</a></li>
                  <li><a href="{{route('admin.settings')}}"><i class="fa fa-cog"></i>Settings</a></li>
                  @endif
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings" href="">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href=""> Profile</a>
                        <a class="dropdown-item"  href="">
                          <span>Settings</span>
                        </a>
                        <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <button type="submit"><i class="fa fa-sign-out pull-right"></i> Log Out</button>
                      </form>
                    </div>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->