@extends('layouts.backend')
@section('title', 'Admin Edit')

@section('content')
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Admin Edit</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                        <form action="{{route('user.update')}}" method="POST" role="form" enctype="multipart/form-data">
                            @csrf
                            <hr>
                            <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="name">Full Name</label>
                                            <input
                                                class="form-control @error('full_name') is-invalid @enderror"
                                                type="text"
                                                id="name"
                                                name="full_name"
                                                value="{{old('full_name', $record->full_name)}}"
                                                required
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> @error('name') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Phone Number</label>
                                            <input
                                                class="form-control @error('phone') is-invalid @enderror"
                                                type="number"
                                                id="phone"
                                                name="phone"
                                                value="{{ old('phone',$record->phone) }}"
                                                required
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> @error('phone') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="name" >User Type</label>
                                            <select name="user_type" id="" class="form-control" required>
                                                <option value="">select user type</option>
                                                @foreach($user_type as $type)
                                                @if($record->user_type == $type)
                                                <option value="{{$type}}" selected>{{$type}}</option>
                                                @else
                                                <option value="{{$type}}" >{{$type}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> @error('user_type') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="name">Password</label>
                                            <input
                                                class="form-control @error('password') is-invalid @enderror"
                                                type="password"
                                                id="password"
                                                name="password"
                                               
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> @error('password') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Email</label>
                                            <input
                                                class="form-control @error('email') is-invalid @enderror"
                                                type="email"
                                                id="email"
                                                name="email"
                                                value="{{ old('email',$record->email) }}"
                                                required
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> @error('email') <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input
                                    class=""
                                    type="hidden"
                                    id="id"
                                    name="id"
                                    value="{{ old('id',$record->id) }}"
                                    required
                                />
                                
                                <div class="tile-footer">
                                    <div class="row d-print-none mt-2">
                                        <div class="col-12 text-right">
                                              <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add User</button>
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection