@extends('layouts.backend')
@section('title', 'Settings')

@section('content')
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark">
                    <h5><i class="fa fa-plus-circle"></i> Manage Settings</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                    @endif
                    <form action="{{route('admin.settings.update')}}" method='POST' class="py-4" enctype="multipart/form-data">  
                       @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Name of School *</label>  
                                    <input type="text" class=" form-control form-control @error('title') is-invalid @enderror" id="exampleFormControlInput1"  name="school_name" value="{{ config('settings.school_name') }}"  required>  
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div> 
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Current Session *</label>  
                                    <input type="text" class=" form-control form-control @error('date') is-invalid @enderror" id=""  value="{{ config('settings.current_session') }}" name="current_session" required>  
                                     @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Phone Number</label>  
                                    <input type="text" class=" form-control form-control @error('time') is-invalid @enderror" id="exampleFormControlInput1"  name="phone_number" value="{{ config('settings.phone_number') }}" >  
                                    @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div> 
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">School Email</label>  
                                    <input type="text" class=" form-control form-control @error('venue') is-invalid @enderror" id="exampleFormControlInput1"  name="email_address" value="{{ config('settings.email_address') }}" >  
                                    @error('venue')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">School Address</label> 
                                    <input type="text" class=" form-control form-control @error('image') is-invalid @enderror" id="exampleFormControlInput1"  name="school_address" value="{{ config('settings.school_address') }}"  >  
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div>
                            <div class="col-sm-6">
                               <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">School Logo</label>  
                                    <input type="file" class=" form-control form-control @error('logo') is-invalid @enderror" id="exampleFormControlInput1"  name="logo" value="" >  
                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">About Us</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="about_us">{{ config('settings.about_us') }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Mission</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="mission">{{ config('settings.mission') }}</textarea>
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Vision</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="vision">{{ config('settings.vision') }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Motto</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="motto">{{ config('settings.motto') }}</textarea>
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Facebook Url</label> 
                                    <input type="text" class=" form-control form-control @error('facebook_url') is-invalid @enderror" id="exampleFormControlInput1"  name="facebook_url" value="{{ config('settings.facebook_url') }}"  >  
                                    @error('facebook_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div>
                            <div class="col-sm-4">
                               <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Instagram Url</label>  
                                    <input type="text" class=" form-control form-control @error('instagram_url') is-invalid @enderror" id="exampleFormControlInput1"  name="instagram_url" value="{{ config('settings.instagram_url') }}" > 
                                    @error('instagram_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div>
                            <div class="col-sm-4">
                               <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Twitter Url</label>  
                                    <input type="text" class=" form-control form-control @error('twitter_url') is-invalid @enderror" id="exampleFormControlInput1"  name="twitter_url" value="{{ config('settings.twitter_url') }}" > 
                                    @error('twitter_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </fieldset>
                            </div>
                        </div>
                        <div class="">
                            <button class="btn btn-lg btn-primary" type="submit"  >Update</button>
                        </div>
                   </form>
                   <div class="text-center bg-secondary">
                        <img height="150px" class="" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{ config('settings.logo') }}"  class="img-fluid" alt="image">
                    </div>
                </div> 
            </div>
        </div>
    </div>
    @push('scripts')
    <script src="{{asset('summernote/assets/summernote-lite.js')}}"></script>
    <script src="{{asset('summernote/assets/lang/summernote-es-ES.js')}}"></script>
    <script>
    $('.summernote').summernote({

    });
    </script>
    
    @endpush
     
    @push('css')
      
    
    <link href="{{asset('summernote/assets/summernote-lite.css')}}" rel="stylesheet"></link>
 
    @endpush

@endsection