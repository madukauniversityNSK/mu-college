<x-backend>
    <x-slot name="title">
    Manage Testimomy
    </x-slot>
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark">
                    <h5><i class="fa fa-plus-circle"></i> Manage Testimomy</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_tile">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Manage Testimonty</a>
                        </li>
                        <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Create Testimonty</a>
                        </li>
                    </ul>
                    <div class="tab-content py-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div>
                             @if (session()->has('message'))
                               <div class="alert alert-success">
                                  {{ session('message') }}
                                </div>
                              @endif
                            </div>
                            <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                   
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card-box table-responsive">
                                                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Descipline</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        @foreach($record as $records)
                                                            <tr>
                                                               
                                                                <td>{{$records->name}}</td>
                                                                <td>{{$records->descipline}}</td>
                                                                <td>
                                                                
                                                                    <a href="{{route('testimony-edit', $records->id)}}"><button  class="btn badge btn-success">Edit</button></a>
                                                                    <a href="{{route('testimony-remove', $records->id)}}"><button  class="btn badge btn-danger">Delete</button></a>
                                                                </td>
                                                               
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <livewire:create-testimony/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-backend>