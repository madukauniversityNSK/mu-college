@extends('layouts.app')

@section('content')
    <style>



        #form-container {
            width: 500px;
        }

        .row {
            margin-top: 15px;
        }
        .row.form-group {
            padding-left: 15px;
            padding-right: 15px;
        }
        .btn {
            margin-left: 15px;
        }

        .change-link {
            background-color: #000;
            border-bottom-left-radius: 6px;
            border-bottom-right-radius: 6px;
            bottom: 0;
            color: #fff;
            opacity: 0.8;
            padding: 4px;
            position: absolute;
            text-align: center;
            width: 150px;
        }
        .change-link:hover {
            color: #fff;
            text-decoration: none;
        }

        img {
            width: 150px;
        }

        #editor-container {
            height: 130px;
        }



    </style>

    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

            <form enctype="multipart/form-data" action="{{ route('store-post') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-4">
                        <img class="img-rounded featured-image"  name="featured-image" id="featured-image" src="{{asset('/img/logo.png')}}">
                        <label for="files" class="btn change-link">Select Feature Image</label>


                        <input type="file"  id="files" style="visibility:hidden;" name="doc" accept="image/jpeg" onchange="readURL(this);" >
                        
                    </div>
                    <div class="col-xs-8">
                        <div class="form-group">
                            <label for="display_name">Category</label>

                            <select class="form-control " id="category_id" name="category_id" >
                                <option value="1" >--Uncategorized --</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" name="title" type="text" placeholder="post title">
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags (optional)</label>
                            <input class="form-control" name="tags" type="text" data-role="tagsinput" placeholder="comma separated. E.g James, Ujah, Paul">
                        </div>

                    </div>
                </div>
                <div class="row form-group">
                    <label for="content">Post Content</label>

                    <div id="standalone-container" >
                        <div id="toolbar-container">
    <span class="ql-formats">
      <select class="ql-font"></select>
      <select class="ql-size"></select>
    </span>
                            <span class="ql-formats">
      <button class="ql-bold"></button>
      <button class="ql-italic"></button>
      <button class="ql-underline"></button>
      <button class="ql-strike"></button>
    </span>
                            <span class="ql-formats">
      <select class="ql-color"></select>
      <select class="ql-background"></select>
    </span>
                            <span class="ql-formats">
      <button class="ql-script" value="sub"></button>
      <button class="ql-script" value="super"></button>
    </span>
                            <span class="ql-formats">
      <button class="ql-header" value="1"></button>
      <button class="ql-header" value="2"></button>
      <button class="ql-blockquote"></button>
      <button class="ql-code-block"></button>
    </span>
                            <span class="ql-formats">
      <button class="ql-list" value="ordered"></button>
      <button class="ql-list" value="bullet"></button>
      <button class="ql-indent" value="-1"></button>
      <button class="ql-indent" value="+1"></button>
    </span>
                            <span class="ql-formats">
      <button class="ql-direction" value="rtl"></button>
      <select class="ql-align"></select>
    </span>
                            <span class="ql-formats">
      <button class="ql-link"></button>
      <button class="ql-image"></button>
      <button class="ql-video"></button>
      <button class="ql-formula"></button>
    </span>
                            <span class="ql-formats">
      <button class="ql-clean"></button>
    </span>
                        </div>
                        <input name="content" type="hidden">
                        <div id="editor-container"  style="min-height: 350px;" >

                        </div>

                    </div>

                </div>
                <div class="row">
                    <button class="btn btn-primary"  type="submit">Publish</button>
                </div>
            </form>

    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.featured-image')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: '#toolbar-container',
            },
            placeholder: 'Compose  post',
            theme: 'snow'
        });



        var form = document.querySelector('form');
        form.onsubmit = function() {
            var myEditor = document.querySelector('#editor-container');
            //var html =
            // Populate hidden form on submit
            var content = document.querySelector('input[name=content]');
            content.value = myEditor.children[0].innerHTML; //JSON.stringify(quill.setContents(content.value));
            return true;

        };







    </script>
@endsection
