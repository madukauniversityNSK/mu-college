@extends('layouts.master')
@section('content')
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="page-banner ovbl-dark" style="background-image:url(assets/front/images/banner/banner1.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white">Staffs</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumb row -->
        <div class="breadcrumb-row">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="#">Home</a></li>
                    <li>Staffs</li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumb row END -->
        <!-- contact area -->
        <div class="content-block">
            <!-- Portfolio  -->
            <div class="section-area section-sp1 gallery-bx">
                <div class="container">
                    <div class="feature-filters clearfix center m-b40">
                        <ul class="filters" data-toggle="buttons">
                            <li data-filter="" class="btn active">
                                <input type="radio">
                                <a href="#"><span>All</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix">
                        <ul id="masonry" class="ttr-gallery-listing magnific-image row">
                            @foreach($staff as $p)
                            <li class="action-card col-lg-3 col-md-4 col-sm-6 book">
                                <div class="ttr-box portfolio-bx">
                                    <div class="ttr-media media-ov2 media-effect">
                                        <a href="javascript:void(0);">
                                            <img src="{{ asset('assets/uploaded') }}/{{ $p->filename }}" alt="" style="height: 300px;">
                                        </a>
                                        <div class="ov-box">
                                            <div class="overlay-icon align-m">
                                                <a href={{ asset('assets/uploaded') }}/{{ $p->filename }}" class="magnific-anchor" title="Title Come Here">
                                                    <i class="ti-search"></i>
                                                </a>
                                            </div>
                                            <div class="portfolio-info-bx bg-primary">
                                                <h4>
                                                    <a href="#">{{$p->name}}</a><br/>
                                                    <a href="#">{{$p->description}}</a>
                                                </h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                           @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->
    @endsection
