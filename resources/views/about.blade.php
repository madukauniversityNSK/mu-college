@extends('layouts.master')

@section('content')
<!-- Inner Content Box ==== -->
<div class="page-content">
    <!-- Page Heading Box ==== -->
    <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg);">
        <div class="container">
            <div class="page-banner-entry">
                <h1 class="text-white">About Us</h1>
            </div>
        </div>
    </div>
    <div class="breadcrumb-row">
        <div class="container">
            <ul class="list-inline">
                <li><a href="/">Home</a></li>
                <li>About Us</li>
            </ul>
        </div>
    </div>
    <!-- Page Heading Box END ==== -->
    <!-- Page Content Box ==== -->
    <div class="content-block">
        <!-- About Us ==== -->
        <div class="section-area section-sp1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
                        <div class="feature-container">
                            <div class="feature-md text-white m-b20">
                                <a href="#" class="icon-cell"><img src="{{asset('assets/front/images/icon/icon1.png')}}" alt=""/></a>
                            </div>
                            <div class="icon-content">
                                <h5 class="ttr-tilte">Our Philosophy</h5>
                                <p>{{$about->philosophy}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
                        <div class="feature-container">
                            <div class="feature-md text-white m-b20">
                                <a href="#" class="icon-cell"><img src="{{asset('assets/front/images/icon/icon2.png')}}" alt=""/></a>
                            </div>
                            <div class="icon-content">
                                <h5 class="ttr-tilte">Our Vision</h5>
                                <p>{{$about->vision}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
                        <div class="feature-container">
                            <div class="feature-md text-white m-b20">
                                <a href="#" class="icon-cell"><img src="{{asset('assets/front/images/icon/icon3.png')}}" alt=""/></a>
                            </div>
                            <div class="icon-content">
                                <h5 class="ttr-tilte">Our Mission</h5>
                                <p>{{$about->mission}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
                        <div class="feature-container">
                            <div class="feature-md text-white m-b20">
                                <a href="#" class="icon-cell"><img src="{{asset('assets/front/images/icon/icon4.png')}}" alt=""/></a>
                            </div>
                            <div class="icon-content">
                                <h5 class="ttr-tilte">Our Motto</h5>
                                <p>{{$about->motto}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About Us END ==== -->
        <!-- Our Story ==== -->
        <div class="section-area bg-gray section-sp1 our-story">
            <div class="container">
                <div class="row align-items-center d-flex">
                    <div class="col-lg-5 col-md-12 heading-bx">
                        <h2 class="m-b10">About Us</h2>
                        <h5 class="fw4"></h5>
                        <p>
                            {{$about->about_text}}
                        </p>
                    </div>
                    <div class="col-lg-7 col-md-12 heading-bx p-lr">
                        <div class="video-bx">
                            @foreach($admin as $a)
                                <img src="storage/logo/{{ $a->logo}}" alt=""/>
                            @endforeach
                            <a href="" class="popup-youtube video"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Story END ==== -->

    </div>
    <!-- Page Content Box END ==== -->
</div>
<!-- Inner Content Box END ==== -->
    @endsection
