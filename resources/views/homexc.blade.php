@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">New Post</div>
                <form class="form-horizontal" method="POST" action="{{ route('store-post') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="item form-group">
                        <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input id="title" type="text" class="form-control col-md-7 col-xs-12" name="title" value="{{ old('title') }}" required>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label for="post_type_id" class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select class="form-control col-md-7 col-xs-12" id="post_type_id" name="post_type_id" >
                                <option value="" >--Select post Type --</option>

                            </select>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label for="content" class="control-label col-md-3 col-sm-3 col-xs-12">Details</label>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea id="content" name="content" class="form-control col-md-7 col-xs-12 my-editor"></textarea>

                        </div>
                    </div>

                    <div class="item form-group">
                        <label for="doc" class="control-label col-md-3 col-sm-3 col-xs-12">Set Featured Image</label>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input id="doc" type="file" class="form-control col-md-7 col-xs-12" name="doc" value="{{ old('doc') }}" autofocus>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Tags</label>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input id="title" type="text" class="form-control col-md-7 col-xs-12" name="tag" placeholder="separate with comma e.g blog, dance, etc." value="{{ old('tag') }}" required>
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-block btn-success">Publish</button>
                        </div>
                    </div>
                </form>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                        {{--<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>--}}
                        <script>
                            // tinymce.init({
                            //     path_absolute : "/",
                            //
                            //     selector:'textarea.my-editor',
                            //     width: 900,
                            //
                            //
                            //     plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
                            //     toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                            //     image_advtab: true,
                            //
                            //
                            // });
                            var quill = new Quill('.my-editor', {
                                modules: { toolbar: 'true' },
                                theme: 'snow'
                            });

                            // var options = {
                            //     debug: 'info',
                            //     modules: {
                            //         toolbar: '#toolbar'
                            //     },
                            //     placeholder: 'Compose an epic...',
                            //     readOnly: true,
                            //     theme: 'snow'
                            // };
                            // var editor = new Quill('.my-editor');
                            // var editor = new Quill('.my-editor', options);
                        </script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
