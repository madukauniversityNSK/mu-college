@extends('layouts.frontend')
@section('title', 'Staff')

@section('content')
<hr style="margin-top: -5px">
<div class=" py-4">
    <h2 class="mb-4 text-center">Our Staffs</h2>
<div>
<section>
    <div class="container py-4">
        <div class="row mb-4">
            @foreach($staff as $p)
            <div class="col-md-4"style="margin-bottom:30px" >
                <div class="card">
                    <img style="height: 300px" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$p->filename}}" class="card-img-top" alt="...">
                    <div class="card-body" >
                        <div class="card-text" >{{$p->name}}</div> 
                        <div class="blockquote-footer font-weight-bold">{{$p->description}}</div>   
                    </div>
                </div>
            </div>
            @endforeach
        <div>  
    </div>
</section>


@endsection