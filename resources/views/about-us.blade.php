@extends('layouts.frontend')
@section('title', 'About Us')

@section('content')
    <hr class=" d-none d-sm-block" style="margin-top: -10px">
    <section class="about_us" >
    <div class="container">            
        <div class="row"  style="margin-top: -180px">
            <div class="col-12 col-sm-12 col-md-7 col-lg-7" style="margin-top: 80px">
                <div class="about_title">
                    <span>About Us</span>
                    <p style="margin-top: 10px">{{ config('settings.about_us') }}</p>
                 </div>
            </div>
            <div class="col-12 col-sm-12 col-md-5 col-lg-5 p-0">
                <div class="banner_about blog d-none d-sm-block"  style="margin-bottom: 70px">
                     @if($photo->filename)
                    <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$photo->filename}}" alt="" class="img-fluid">
                    @endif
                </div>
                <div class="banner_about blog d-block d-sm-none"  style="margin-top: -150px">
                    @if($photo->filename)
                    <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$photo->filename}}" alt="" class="img-fluid">
                    @endif
                </div>
            </div>
        </div>        
    </div>    
</section>


<!--========={ Popular Courses }========-->
<section class="unlimited_possibilities" id="about_unlimited_possibilities"  style="margin-top: 30px">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12" >
                <div class="sub_title">
                    <h2  >Unlimited Possibilities</h2> 
                </div><!-- ends: .section-header -->
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                 <div class="single_item single_item_first">
                    <div class="icon_wrapper">
                        <i class="flaticon-student"></i>
                    </div>
                    <div class="blog_title">
                        <h3><a href="#" title="">Our Vision</a></h3> 
                        <p>{{ config('settings.vision') }}</p>                    
                    </div>   
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                <div class="single_item single_item_center">
                    <div class="icon_wrapper">
                        <i class="flaticon-university"></i>
                    </div>
                    <div class="blog_title">
                        <h3><a href="#" title="">Our Mission</a></h3> 
                        <p>{{ config('settings.mission') }}</p>                    
                    </div>   
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
               <div class="single_item single_item_last">
                <div class="icon_wrapper">
                        <i class="flaticon-diploma"></i>
                    </div>
                    <div class="blog_title">
                        <h3><a href="#" title="">Our Motto</a></h3> 
                        <p>{{ config('settings.motto') }}</p>
                    </div>   
                </div>
            </div>                        
        </div>
    </div>
</section><!-- End Popular Courses -->




@endsection