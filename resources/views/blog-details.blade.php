@extends('layouts.frontend')
@section('title', 'Blog-Details')

@section('content')

<hr class=" d-none d-sm-block" style="margin-top: -5px">
<section class="blog_wrapper" style="margin-top:-100px">
    <div class="container">  
        <div class="row" >        
            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                <div class="blog_post">
                    <h3>{{$record->title}}</h3>
                    <div class="post_by d-flex">
                        <span>By - <a href="#" title="" class="bloger_name">Admin</a></span>
                        <span>Posted On : {{ date('d F Y',strtotime($record->created_at))}}</span>                        
                    </div>
                    <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$record->filename}}" alt="" class="img-fluid">
                    <div class="postpage_content_wrapper">
                        <div class="blog_post_content">
                            {{Illuminate\Mail\Markdown::parse($record->content)}}
                        </div>
                    </div>
                </div>               
            </div> <!-- End Blog Left Side-->

            <div class="col-12 col-sm-12 col-md-4 col-lg-4 blog_wrapper_right ">
                <div class="blog-right-items">
                    <div class="recent_post_wrapper widget_single">
                        <div class="items-title">
                            <h3 class="title">Recent Post</h3>
                        </div>
                        @foreach($recents as $recent)
                        <div class="single-post">
                            <div class="recent_img">
                                 <a href="{{route('blog-details',$recent->slug)}}" title=""><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/{{$recent->filename}}" alt="" class="img-fluid"></a>
                            </div>
                           
                            <div class="post_title">
                                <a href="{{route('blog-details',$recent->slug)}}" title="">{{$recent->title}}</a>
                                <div class="post-date">
                                    <span>{{ date('d F Y',strtotime($recent->created_at))}}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div><!-- ./ End  Blog Right Side-->
            
        </div>
    </div> 
</section> <!-- ./ End Blog Area Wrapper-->

@endsection