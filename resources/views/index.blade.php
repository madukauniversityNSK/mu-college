@extends('layouts.app')

@section('content')
    <div class="page-cont" style="padding: 20px;">
    <div class="row">
        <div class="col-md-4">
            <div class="item">
                <div class="service-bx">
                    <div class="info-bx d-flex">
                        <div>
                            <div class="event-time">
                                <div class="event-date">{{$posts}}</div>
                                <div class="event-month"></div>
                            </div>
                        </div>
                        <div class="event-info">
                            <h4 class="event-title"><a href="#">Posts</a></h4>
                            <ul class="media-post">
                                <li><a href="{{url('all-posts')}}"><i class="fa fa-map-marker"></i> View All</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
                <div class="service-bx">
                    <div class="info-bx d-flex">
                        <div>
                            <div class="event-time">
                                <div class="event-date">{{$gallery}}</div>
                                <div class="event-month"></div>
                            </div>
                        </div>
                        <div class="event-info">
                            <h4 class="event-title"><a href="#">Gallery Images</a></h4>
                            <ul class="media-post">
                                <li><a href="{{url('all-gallery')}}"><i class="fa fa-map-marker"></i> View All</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="item">
                <div class="service-bx">
                    <div class="info-bx d-flex">
                        <div>
                            <div class="event-time">
                                <div class="event-date">{{$slider}}</div>
                                <div class="event-month"></div>
                            </div>
                        </div>
                        <div class="event-info">
                            <h4 class="event-title"><a href="#">Slider Images</a></h4>
                            <ul class="media-post">
                                <li><a href="{{url('all-sliders')}}"><i class="fa fa-map-marker"></i> View All</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <br/>
        <div class="row">
            <div class="col-md-4">
                <div class="item">
                    <div class="service-bx">
                        <div class="info-bx d-flex">
                            <div>
                                <div class="event-time">
                                    <div class="event-date">{{$events}}</div>
                                    <div class="event-month"></div>
                                </div>
                            </div>
                            <div class="event-info">
                                <h4 class="event-title"><a href="#">Events</a></h4>
                                <ul class="media-post">
                                    <li><a href="{{url('all-event')}}"><i class="fa fa-map-marker"></i> View All</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="service-bx">
                        <div class="info-bx d-flex">
                            <div>
                                <div class="event-time">
                                    <div class="event-date">{{$users}}</div>
                                    <div class="event-month"></div>
                                </div>
                            </div>
                            <div class="event-info">
                                <h4 class="event-title"><a href="#">Users</a></h4>
                                <ul class="media-post">
                                    <li><a href="#"><i class="fa fa-map-marker"></i> View All</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="service-bx">
                        <div class="info-bx d-flex">
                            <div>
                                <div class="event-time">
                                    <div class="event-date">{{$staffs}}</div>
                                    <div class="event-month"></div>
                                </div>
                            </div>
                            <div class="event-info">
                                <h4 class="event-title"><a href="#">Staffs</a></h4>
                                <ul class="media-post">
                                    <li><a href="{{url('all-staff')}}"><i class="fa fa-map-marker"></i> View All</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
