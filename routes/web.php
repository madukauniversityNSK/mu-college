<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@welcome')->name('index');
Route::get('about', 'HomeController@about')->name('about');
Route::get('staffs','HomeController@staffz')->name('staffs');
Auth::routes();



// Admin Automation for logo and name



Route::group(['middleware' => 'auth'], function () {

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/settings', 'settingController@index')->name('admin.settings');
Route::post('settings-update', 'settingController@update')->name('admin.settings.update');


//post
Route::get('adimg', 'AdimgController@index');
Route::post('addimage', 'SiteController@saveSlider')->name('addimage');

Route::get('create-post', 'PostController@create')->name('create-post');
Route::post('/post/store', 'PostController@store')->name('store-post');


Route::get('all-posts', 'PostController@show')->name('all-posts');
Route::get('edit-post/{id}', 'PostController@edit')->name('edit-post');
Route::get('delete-post/{id}', 'PostController@destroy')->name('delete-post');
Route::post('update-post', 'PostController@update')->name('update-post');

Route::post('/post/delete', 'PostController@delete')->name('delete-post');

//slider
Route::get('add-slider', 'SiteController@addslider')->name('add-slider');
Route::get('all-sliders', 'SiteController@showslider')->name('all-sliders');
Route::get('delete-slider/{id}', 'SiteController@deleteslider')->name('delete-slider');
Route::get('edit-slider/{id}', 'SiteController@editslider')->name('edit-slider');
Route::post('save-slider', 'SiteController@saveSlider')->name('save-slider');
Route::post('update-slider', 'SiteController@updateSlider')->name('update-slider');


//gallery
Route::get('add-gallery', 'SiteController@addgallery')->name('add-gallery');
Route::post('save-gallery', 'SiteController@saveGallery')->name('save-gallery');
Route::get('all-gallery', 'SiteController@showgallery')->name('all-gallery');
Route::get('delete-gallery/{id}', 'SiteController@deletegallery')->name('delete-gallery');
Route::get('edit-gallery/{id}', 'SiteController@editgallery')->name('edit-gallery');
Route::post('update-gallery', 'SiteController@updateGallery')->name('update-gallery');

//event
Route::get('add-event', 'SiteController@addevents')->name('add-event');
Route::post('save-event', 'SiteController@saveEvent')->name('save-event');
Route::get('all-event', 'SiteController@showevent')->name('all-event');
Route::get('delete-event/{id}', 'SiteController@deleteevent')->name('delete-event');
Route::get('edit-event/{id}', 'SiteController@editevent')->name('edit-event');
Route::post('update-event', 'SiteController@updateEvent')->name('update-event');

//staff
Route::get('add-staff', 'SiteController@addstaff')->name('add-staff');
Route::post('save-staff', 'SiteController@savestaff')->name('save-staff');
Route::get('all-staff', 'SiteController@showstaff')->name('all-staff');
Route::get('delete-staff/{id}', 'SiteController@deletestaff')->name('delete-staff');
Route::get('edit-staff/{id}', 'SiteController@editstaff')->name('edit-staff');
Route::post('update-staff', 'SiteController@updatestaff')->name('update-staff');

//ABOUT
Route::get('all-about', 'SiteController@showabout')->name('all-about');
Route::get('edit-about/{id}', 'SiteController@editabout')->name('edit-about');
Route::post('update-about', 'SiteController@updateabout')->name('update-about');

//Menu
Route::get('/menu/create', 'MenuController@create')->name('create-menu');
Route::post('/menu/store', 'MenuController@store')->name('store-menu');

Route::get('/menu/edit', 'MenuController@edit')->name('edit-menu');
Route::post('/menu/update', 'MenuController@update')->name('update-menu');

Route::post('/menu/type/store', 'MenuController@typeStore')->name('store-menu-type');
Route::post('/menu/location/store', 'MenuController@locationStore')->name('store-menu-location');

Route::post('/post/delete', 'MenuController@delete')->name('delete-menu');


//Documents
Route::get('/document/create', 'DocumentController@create')->name('create-document');
Route::post('/document/store', 'DocumentController@store')->name('store-document');

Route::get('/document/edit', 'DocumentController@edit')->name('edit-document');
Route::post('/document/update', 'DocumentController@update')->name('update-document');

Route::post('/document/delete', 'DocumentController@delete')->name('delete-document');

//socials
Route::get('/social/create', 'SocialController@create')->name('create-social');
Route::post('/social/store', 'SocialController@store')->name('store-social');

Route::get('/social/edit', 'SocialController@edit')->name('edit-social');
Route::post('/social/update', 'SocialController@update')->name('update-social');

Route::post('/social/delete', 'SocialController@delete')->name('delete-social');

//Site-details
Route::get('/site/edit', 'SiteController@edit')->name('edit-site');
Route::post('/site/update', 'SiteController@update')->name('update-site');

Route::post('/site/delete', 'SiteController@delete')->name('delete-site');
Route::get('single/{id?}', 'SiteController@single')->name('single');


//Tags
Route::get('/tag/edit', 'TagController@edit')->name('edit-tag');
Route::post('/tag/update', 'TagController@update')->name('update-tag');

Route::post('/tag/delete', 'TagController@delete')->name('delete-tag');

});


Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
    
    Route::get('/create', 'UserController@create')->name('user.create');
    Route::post('/create', 'UserController@save')->name('user.create');
    Route::get('/', 'UserController@index')->name('user.index');
    Route::get('/delete/{id}', 'UserController@delete')->name('user.delete');
    Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
    Route::post('/update', 'UserController@update')->name('user.update');
});

Route::get('blog/details/{slug}', 'PostController@blog_detail')->name('blog-details');
Route::get('blog/list', 'PostController@blog_list')->name('blog-list');
