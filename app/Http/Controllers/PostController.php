<?php

namespace App\Http\Controllers;

use App\Category;
use App\Document;
use App\FeaturedImage;
use App\Post;
use App\PostDocument;
use App\Tag;
use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Auth;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    public function store(Request $request)
    {
       //return $request;

        DB::transaction(function(){
            $post= new Post();
            $post->content = request('content');
            $post->title = request('title');
            $post->user_id = Auth::user()->id;
            $post->slug= request('title');
            if(request('image'))
            {
                $image = request('image');
                $filename = Storage::disk('s3')->put('mu-gallery/post', $image);
                $post['filename'] = $filename;
                $post['mime'] = $image->getClientMimeType();
            }
            $post->save();

            if ( request('tags'))
            {
                $tags = explode (",", request('tag'));

                foreach($tags as $tag){
                    $new_tag = new Tag();
                    $new_tag->post_id = $post->id;
                    $new_tag->tag = trim($tag);
                    $new_tag->save();

                }
            }
//            if( request()->hasFile('featured-image') )
//            {
//                $path = Storage::putFile('public/documents', request()->file('doc'));
//                $path = substr($path, 6);
//                $document = new Document();
//                $document->path = $path;
//                $document->type = 2;
//                $document->disk = Config::get('filesystems.default');
//                $document->slug = $post->slug;
//                $document->save();
//
//                $fi = new FeaturedImage();
//                $fi->post_id = $post->id;
//                $fi->document_id = $document->id;
//                $fi->save();
//            }

        }, 5);

        session()->flash('message', 'Post Created Successfully.');
        return redirect('all-posts');
    }

    public function create()
    {
        $categories = Category::all();

        return view('backend.post.index')->with('categories', $categories);

    }

    public function edit($id)
    {
        $record= Post::findorfail($id);
        return view('backend.post.edit')->with('record', $record);

    }

    public function update( Request $request)
    {
        $sli= Post::whereid($request->id)->first();
        $sli->title = $request->title;
        $sli->content = $request->content;
        if($request->hasFile('image'))
        {   
            Storage::disk('s3')->delete($sli->filename);
            $image = request('image');
            $filename = Storage::disk('s3')->put('mu-gallery/post', $image);
            $sli['filename'] = $filename;
            $sli['mime'] = $image->getClientMimeType();
        }
        $sli->save();
        session()->flash('message', 'Post Updated Successfully.');
        return redirect('all-posts');
    }

    public function show()
    {
        $record = Post::orderBy('id','Desc')->get();
        return view('backend.post.index',compact('record'));
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        Storage::disk('s3')->delete($post->filename);
        session()->flash('message', 'Post Deleted Successfully.');
        return redirect('all-posts');
    }
    
    public function blog_detail(request $request)
    {   
        $recents = Post::orderBy('id','Desc')->take(5)->get();
        $record = Post::where('slug', $request->slug)->first();
        return view('blog-details',compact('record', 'recents'));
    }

    public function blog_list(request $request)
    {   
        $records = Post::orderBy('id','Desc')->paginate(12);

        return view('blog-list',compact('records'));
    }
}
