<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Intervention\Image\Facades\Image;
use App\Admin;
use App\Event;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() 
    {
        // $this->middleware('auth');
    }
    public function index()
    {
        $data['admin'] = Admin::all();

        return view('admin.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }
 
/**
 * Save slider
 * @return Back
 */
    public function saveEvent( Request $request)
    {
        if($request->hasFile('filename'))
        {
            $event = new Event();
            $event->event_name = $request->name;
            $event->start_date = $request->start;
            $event->end_date = $request->end;
            $event->event_description = $request->description;
            $event->location = $request->location;
            $image = $request->file('filename');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = 'assets/uploaded/' . $filename;
           
            Image::make($image)->save($location);
        
            $event['filename'] = $filename;
            $event['mime'] = $image->getClientMimeType();
            
            $event->save();
        }
        session()->flash('message.level', 'success');
        session()->flash('message.content', 'Successful.');
        return back()->with('#session');
     }

     
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $this->validate($request,[
            'school_name' => 'required',
            'logo' => 'image|max:1999'
        ]);
        
        $image = $request->file('logo');
        $filename = time().'.'.$image->getClientOriginalExtension();
        $location = 'assets/uploaded/' . $filename;
        Image::make($image)->save($location);


        
     
        $admin = new Admin;
        $admin->school_name = $request->input('school_name');
        $admin->logo = $filename;
        $admin->save();

        return redirect('admin');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['admin'] = Admin::find($id);

        return view('admin.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['admin'] =  Admin::findOrFail($id);

        return view('admin.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $fileNameWithExt = $request->file('logo')->getClientOriginalName();
        
        $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

        $extension = $request->file('logo')->getClientOriginalExtension();

        $fileNameToStore = $filename.'_'.time().'.'.$extension;

        $path = $request->file('logo')->storeAs('public/logo',$fileNameToStore);

        $admin = Admin::find($request->id);
        $admin->school_name = $request->input('school_name');
        $admin->logo = $fileNameToStore;
        $admin->save();

        return redirect('admin')->with('success','School name and Logo Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
