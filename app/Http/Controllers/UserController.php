<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    //

    public function index(Request $request)
    {   
        if(auth()->user()->user_type == "super_admin") {
             $record = User::all();
             return view('backend.users.index',compact('record'));
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if(auth()->user()->user_type == "super_admin") {
        return view('admin.users.create');
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {   
        if(auth()->user()->user_type == "super_admin") {
            $input = $request->except('token');
            $input['password'] = bcrypt($input['password']);
    
            $user = User::create($input);
            if($user) {
                Session()->flash('message', 'User Added Successfully');
                return redirect('user');
            }
        }
    }
    
  
    public function show($id)
    {   
        if(auth()->user()->user_type == "super_admin"){
            $user = User::find($id);
            return view('backend.users.show',compact('user'));
        }
    }
    
    public function edit(request $request)
    {   
        if(auth()->user()->user_type == "super_admin"){
            $user_type = ['admin', 'super_admin'];
            $record = User::find($request->id);
        
            return view('backend.users.edit',compact('record', 'user_type'));
        }
    }
    
    public function update(Request $request)
    {   
        if(auth()->user()->user_type == "super_admin"){
            $data = $request->except('token','id', 'password');
            if ($request->password) {
                $data['password'] = bcrypt($request->password);
            }
            $user = User::find($request->id);
            $user->update($data); 
            if($user) {
                Session()->flash('message','User Updated successfully');
                return redirect('user');
            } 
        } 
    }
    
    public function delete($id)
    {   
        if(auth()->user()->user_type == "super_admin"){
            $user = User::find($id)->delete();
            if ($user) {

                Session()->flash('message','User Updated successfully');

                return redirect('user');
            }
        }
    }
}
