<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\setting;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controllers;

class settingController extends Controller
{
    //

    public function index()
    {    
        if(auth()->user()->user_type == "super_admin") { 
            return view('backend.settings.index');
        }
    }

    public function update(Request $request) {    
        if(auth()->user()->user_type == "super_admin") {
            $keys = $request->except('_token','logo');
            if ($request->logo) {
                Storage::disk('s3')->delete(config('settings.logo'));
                $logo = Storage::disk('s3')->put('maduka-college/logo', $request->logo);
                $keys['logo'] = $logo;
       
            }

            foreach ($keys as $key => $value) {
                setting::set($key, $value);
            }
            Session()->flash('message', 'Record Updated Successfully');

            return redirect('admin/settings');
        }
    }
}
