<?php

namespace App\Http\Controllers;

use App\Menu;
use App\MenuLocation;
use App\MenuType;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function store(Request $request)
    {
        $menu = new Menu();
        $menu->parent_id = request('parent_id');
        $menu->display = request('display');
        $menu->url = request('menu_url');

        $menu->type_id = request('type_id');

        $menu->save();

        $status = 'Operation Successful';

        return back()->with('status', $status);

    }

    public function  typeStore(Request $request)
    {
        $type= new MenuType();
        $type->name = request('name');
        $type->save();

        $status = 'Operation Successful';

        return back()->with('status', $status);
    }

    public function locationStore()
    {
        $location= new MenuLocation();
        $location->name = request('name');
        $location->save();

        return back();

    }

    public function create()
    {
        $menus = Menu::all();
        $menu_types = MenuType::all();
        $menu_location = MenuLocation::all();

        return view('admin.menu.create')->with('menus', $menus)
            ->with('menu_locations', $menu_location)
            ->with('menu_types', $menu_types);

    }

    public function edit(Request $request)
    {

    }

    public function update( Request $request)
    {

    }

    public function show(Request $request)
    {

    }
}
