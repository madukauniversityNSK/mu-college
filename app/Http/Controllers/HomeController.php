<?php

namespace App\Http\Controllers;

use App\About;
use App\Category;
use App\Event;
use App\Gallery;
use App\Post;
use App\Slider;
use App\Staff;
use App\User;
use App\Admin;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');

    }

    public function welcome()
    {
        $data['slider'] = Slider::all();
        $data['gallery'] = Gallery::all();
        $data['posts'] = Post::orderBy('id', 'Desc')->take(3)->get();
        $data['events'] = Event::orderBy('id', 'Desc')->get();
        $data['photo'] = Gallery::orderBy('id','Asc')->take(8)->get();
        $data['gallery'] = Gallery::orderBy('id','Desc')->take(8)->get();
        $data['admin'] = Admin::all();
                
        return view('home',$data);
    }
    public function about()
    {   
        $data['photo'] = Gallery::first();
        $data['about'] = About::first();
        $data['admin'] = Admin::all();

        return view('about-us',$data);
    }
    public function staffz()
    {
        $data['staff'] = Staff::all();
        $data['admin'] = Admin::all();
        return view('teachers',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['posts'] = Post::count();
        $data['gallery'] = Gallery::count();
        $data['slider'] = Slider::count();
        $data['events'] = Event::count();
        $data['users'] = User::count();
        $data['staffs'] = Staff::count();
        
        return view('backend.dashboard',$data);
    }
}
