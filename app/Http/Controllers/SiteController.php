<?php

namespace App\Http\Controllers;

use App\About;
use App\Event;
use App\Gallery;
use App\Post;
use App\Slider;
use App\Staff;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use mysql_xdevapi\Session;
use Illuminate\Support\Facades\Storage;

class SiteController extends Controller
{
    public function addslider()
    {
        return view('add-slider');
    }

    public function saveSlider( Request $request)
    {
        if($request->hasFile('image'))
        {
            $slider = new Slider();
            $slider->title = $request->title;
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('mu-gallery/slider', $image);
          
            $slider['filename'] = $filename;
            $slider['mime'] = $image->getClientMimeType();
            $slider->save();
        }
        session()->flash('message', 'Slider Updated Successfully.');
        return redirect('all-sliders');
    }
    public function showslider()
    {
        $record = Slider::all();
        return view('backend.slider.index',compact('record'));
    }
    public function editslider($id)
    {
        $record = Slider::findorfail($id);
        return view('backend.slider.edit',compact('record'));
    }
    public function updateSlider(Request $request)
    {
        $sli= Slider::whereid($request->id)->first();
        $sli->title = $request->title;
        if($request->hasFile('image'))
        {   
            Storage::disk('s3')->delete($sli->filename);
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('mu-gallery/slider', $image);
            $sli->filename = $filename;
            
        }
        $sli->save();
        session()->flash('message', 'Slider Update Successfully.');
        return redirect('all-sliders');
    }
    public function addgallery()
    {
        return view('add-gallery');
    }
    public function saveGallery( Request $request)
    {
        if($request->hasFile('image'))
        {
            $gallery = new Gallery();
            $gallery->title = $request->title;
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('maduka-college/gallery', $image);
            $gallery['filename'] = $filename;
            $gallery['mime'] = $image->getClientMimeType();
            $gallery->save();
        }
        session()->flash('message', 'Gallery Added Successfully.');
        return redirect('all-gallery');
    }
    public function showgallery()
    {
        $record = Gallery::all();
        return view('backend.gallery.index',compact('record'));
    }
    public function editgallery($id)
    {
        $record = Gallery::findorfail($id);
        return view('backend.gallery.edit',compact('record'));
    }
    public function updateGallery(Request $request)
    {
        $sli= Gallery::whereid($request->id)->first();
        $sli->title = $request->title;
        if($request->hasFile('image'))
        {   
            Storage::disk('s3')->delete($sli->filename);
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('maduka-college/gallery', $image);
            $sli['filename'] = $filename;
            $sli['mime'] = $image->getClientMimeType();
        }
        $sli->save();
        session()->flash('message', 'Gallery Added Successfully.');
        return redirect('all-gallery');
    }
    public function addstaff()
    {
        return view('add-staff');
    }
    public function savestaff( Request $request)
    {
        if($request->hasFile('image'))
        {
            $gallery = new Staff();
            $gallery->name = $request->name;
            $gallery->description = $request->description;
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('maduka-college/staffs', $image);
            $gallery['filename'] = $filename;
            $gallery['mime'] = $image->getClientMimeType();
            $gallery->save();
        }
        session()->flash('message', 'Staff Added Successful.');
        return redirect('all-staff');
    }
    
    public function showstaff()
    {
        $data['record'] = Staff::all();
        return view('backend.staff.index', $data);
    }
    public function editstaff($id)
    {
        $record= Staff::findorfail($id);
        return view('backend.staff.edit',compact('record'));
    }
    public function updatestaff(Request $request)
    {
        $sli= Staff::whereid($request->id)->first();
        $sli->name = $request->name;
        $sli->description = $request->description;
        if($request->hasFile('image'))
        {   
            Storage::disk('s3')->delete($sli->filename);
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('maduka-college/staffs', $image);
            $sli['filename'] = $filename;
            $sli['mime'] = $image->getClientMimeType();
        }
        $sli->save();
        session()->flash('message', 'Staff Updated Successful.');
        return redirect('all-staff');
    }
    public function addevents()
    {
        return view('add-event');
    }
    public function saveEvent( Request $request)
    {
        if($request->hasFile('image'))
        {
            $event = new Event();
            $event->event_name = $request->event_name;
            $event->start_date = $request->start_date;
            $event->end_date = $request->end_date;
            $event->event_description = $request->event_description;
            $event->location = $request->location;
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('muc-event', $image);
            $event['filename'] = $filename;
            $event['mime'] = $image->getClientMimeType();
            $event->save();
        }
        session()->flash('message', 'Event Added Successfully.');
        return redirect('all-event');
    }
    public function showevent()
    {
        $record = Event::all();
        return view('backend.event.index',compact('record'));
    }
    public function editevent($id)
    {
        $record = Event::findorfail($id);
        return view('backend.event.edit',compact('record'));
    }
    public function updateEvent(Request $request)
    {
        $sli= Event::whereid($request->id)->first();
        $sli->event_name = $request->event_name;
        $sli->event_description = $request->event_description;
        $sli->location = $request->location;
        $sli->start_date = $request->start_date;
        $sli->end_date = $request->end_date;
        if($request->hasFile('image'))
        {   
            Storage::disk('s3')->delete($sli->filename);
            $image = $request->file('image');
            $filename = Storage::disk('s3')->put('muc-event', $image);
            $sli['filename'] = $filename;
            $sli['mime'] = $image->getClientMimeType();
        }
        $sli->save();
        session()->flash('message', 'Event Updated Successfully.');
        return redirect('all-event');
    }
    public function deleteslider($id)
    {
        $slid = Slider::findorfail($id);
        $slid->delete();
        Storage::disk('s3')->delete($slid->filename);
        session()->flash('message', 'Slider Deleted Successfully.');
        return redirect('all-sliders');
    }
    public function deleteevent($id)
    {
        $slid = Event::findorfail($id);
        $slid->delete();
        Storage::disk('s3')->delete($slid->filename);
        session()->flash('message', 'Event Deleted Successfully.');
        return redirect('all-event');;
    }
    public function deletegallery($id)
    {
        $slid = Gallery::findorfail($id);
        $slid->delete();
        Storage::disk('s3')->delete($slid->filename);
        session()->flash('message', 'Gallery Deleted Successfully.');
        return redirect('all-gallery');
    }
    public function deletestaff($id)
    {
        $slid = Staff::find($id);
        $slid->delete();
        Storage::disk('s3')->delete($slid->filename);
        session()->flash('message', 'Staff Deleted Successfully.');
        return redirect('all-staff');
    }
    public function showabout()
    {
        $data['about'] = About::all();
        return view('all-about',$data);
    }
    public function editabout($id)
    {
        $data['about'] = About::findorfail($id);
        return view('edit-about',$data);
    }
    public function updateabout(Request $request)
    {
        $sli= About::whereid($request->id)->first();
        $sli->about_text = $request->about;
        $sli->mission = $request->mission;
        $sli->vision = $request->vision;
        $sli->philosophy = $request->philosophy;
        $sli->motto = $request->motto;
        $sli->save();
        session()->flash('message.level', 'success');
        session()->flash('message.content', 'About texts Updated Successfully.');
        return back()->with('#session');
    }
    public function single($id)
    {
        $blog = Post::findorfail($id);
        return view('single',compact('blog'));
    }
}
