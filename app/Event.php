<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table = 'events';
    protected $fillable = ['event_name','event_description','start_date','end_date','location','filename','mime'];
}
