<?php

namespace App\Providers;

use Schema;
use Config;
use App\setting;
use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind('settings', function ($app) {
            return new setting();
        });
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Setting', setting::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //

        // only use the Settings package if the Settings table is present in the database
       if (!\App::runningInConsole() && count(Schema::getColumnListing('settings'))) {
            $settings = setting::all();

            foreach ($settings as $key => $setting)
            {
                  Config::set('settings.'.$setting->key, $setting->value);
            }
        }
    }
}
