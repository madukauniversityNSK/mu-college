<?php
/**
 * configuration setting for website
 */

return [
    'AppName' => 'Maduka University College',
    'AppEmail' => 'enquiries@Madukacollege.com.ng',
    'AppMobile' => '',
    'AppAddress' => '',
    'url' => 'https://mucollege.com.ng',
    'AppNameAlias' => 'Maduka University College',
    'AppPortal' => 'https://portal.mucollege.com.ng',
    'AppMeta' => 'Maduka University College',
    'logo' => 'mu-logo-7.png',
    'logo2' => 'maduka-logo2.png',
    'icon' => 'maduka-logo.png',
];
