$(function () {
    $('div.categories').parents('form')
            .submit(function (e) {
                if (!$('[name="categories[]"]').length) {
                    notify($('div.categories .notify'), {status: false, message: 'You must select at least one category'});
                    e.preventDefault();
                    return false;
                }
                return true;
            });

//    $('div.chip').each(function () {
//        adjustChip(this);
//    });
});

function adjustChip(chip) {
    var input = $('input', chip);
    var width = (((input.val().length + 1) * 7) + 10) + 'px';
    $('input', chip).css('width', width);
}

$('div.chip .item button').on('click', removeChip);
function removeChip(e) {
    e.preventDefault();
    $(this).parents('div.chip').remove();
}

$('.categories > button').click(function (e) {
    e.preventDefault();
    /*Create chip*/
    var modal = $('#catsModal');
    resetCatModal(modal);
    modal.modal({backdrop: 'static', show: true, keyboard: false});
});

$('#catsModal button.ok').click(function () {
    var chip = $(getSelectedChip());
    chip.find('button').on('click', removeChip);
    $('.categories > button').before(chip);
    $('#catsModal').modal('hide');
});

var selectedCat;
$('.cat-filter-box select').change(function () {
    if (!this.value) {
        return;
    }

    selectedCat = {
        'id': this.value,
        'slug': $('option:selected', this).attr('id'),
        'name': $('option:selected', this).text()
    };
    $(this).parents('.cat-filter-box').nextAll('.cat-filter-box').remove();
    /*Get category*/
    $('#catsModal img.loader').show();
    ajaxCall({
        url: fetchCatUrl,
        data: {'slug': selectedCat.slug},
        onSuccess: function (xhr) {
            addCatToModal(xhr);
        },
        onFailure: function (xhr) {
            handleHttpErrors(xhr);
        },
        onComplete: function () {
            $('#catsModal img.loader').hide();
            $('#catsModal button.ok').prop('disabled', false);
        }
    });
});

function getSelectedChip() {
    return '<div class="chip">'
            + '<input value="' + selectedCat.id + '"'
            + 'hidden name="categories[]"/>'
            + '<div class="item"><span>'
            + selectedCat.slug.replace(/\-/g, " ")
            .replace(/\b\S/g, function (t) {
                return t === 'in' ? t : t.toUpperCase();
            })
            + '</span>'
            + '<button class="btn fa fa-times" type="button"></button>'
            + '</div></div>';
}

function addCatToModal(category) {
    var modal = $('#catsModal');
    /*If cat has children*/
    if (typeof (category.children) !== 'object' || category.children.length < 1) {
        return;
    }
    /*clone pane */
    var panel = $('.cat-filter-box:first-child', modal).clone(true).attr('clone', true);
    /*Set name*/
    panel.find('p').text('Categories in ' + category.name);
    /*Loop through children*/
    var select = panel.find('select');
    var options = '<option value="">Select a category</option>';
    for (var ikey in category.children) {
        var child = category.children[ikey];
        options += '<option id="' + child.slug + '" value="' + child.id + '" '
                + '>' + child.name + '</option>';
    }
    select.html(options);
    $('#cat-filter-panel', modal).append(panel);
}

function resetCatModal(modal) {
    $('img.loader', modal).hide();
    $('button.ok', modal).prop('disabled', true);
    $('.cat-filter-box[clone]', modal).remove();
}
