
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    buildCatFilter();
});

$('.cat-filter-box select').change(function () {
    if (!this.value) {
        return;
    }

    selectedCat = this.value;
    $(this).parents('.cat-filter-box').nextAll('.cat-filter-box').remove();
    /*Get category*/
    showPageLoader();
    ajaxCall({
        url: fetchCatUrl,
        data: {'slug': selectedCat},
        onSuccess: function (xhr) {
            addCatToPanel(xhr);
        },
        onFailure: function (xhr) {
            handleHttpErrors(xhr);
        },
        onComplete: function () {
            hidePageLoader();
        }
    });
});

$('#cat-filter-panel ~ button').click(function (e) {
    e.preventDefault();
    window.location = HomeUrl + selectedCat;
});

var selectedCat = '#';
function buildCatFilter() {
    $('#cat-filter-panel').empty();
    for (var index in categories) {
        var category = categories[index];
        addCatToPanel(category);
    }
}

function addCatToPanel(category) {
    /*If cat has children*/
    if (typeof (category.children) !== 'object' || category.children.length < 1) {
        return;
    }
    /*clone pane */
    var panel = $('.cat-filter-box:hidden').clone(true).prop('hidden', false);
    /*Set name*/
    panel.find('p').append(category.name);
    /*Loop through children*/
    var select = panel.find('select');
    var options = '<option value="">Select a category</option>';
    for (var ikey in category.children) {
        var child = category.children[ikey];
        var selected = false;
        for (var j in categories) {
            if (child.id === categories[j].id) {
                selected = true;
                selectedCat = child.slug;
                break;
            }
        }
        options += '<option id="' + child.id + '" value="' + child.slug + '" ' + (selected ? 'selected' : '') + '>' + child.name + '</option>';
    }
    select.html(options);
    $('#cat-filter-panel').append(panel);
}

 $('div a.add-to-cart').click(function (e) {
        e.preventDefault();
        showPageLoader();
        var attr = $(this).attr('href');
        ajaxCall({
            url: attr,
            method: 'POST',
            onSuccess: function (xhr) {

                if (xhr.status == true) {
                    hidePageLoader();
                    toast(xhr.message, 6000);
                }

            },
            onFailure: function () {
                toast('oops, something went wrong', 6000);
            },
            onComplete: function () {
                hidePageLoader();
                location.reload();
            }

        });

    });