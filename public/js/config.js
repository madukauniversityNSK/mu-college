var ccu = {};

// plugin loading setup
(function () {
    var preset = {
                jquery: '//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'
            };

    /**
     * ccu.import - loadjs wrapper that simplifies dependency importing with presets and auto-page checking
     * @param {string/array} ids Name(s) of plugin(s) to load
     * The following additional params may be overloaded
     * @param {string/object} Selector/Element to check if exists on page 
     * @param {boolean} Load asyncronously
     * @param {function} callback
     */
    ccu.import = function (ids) {

        var deps = typeof ids === 'string' ? ids.split(',') : typeof ids === 'object' ? ids : false,
                el = false, async = false, callback = false, urls = false,
                checked = [], i,
                check = function (dep) {
                    var ret = [], chk, i;

                    // check string
                    if (typeof dep === 'string') {
                        if (checked.indexOf(dep) === -1) { // stop duplicates/infinite loop
                            checked.push(dep);
                            // check against preset
                            if (typeof preset[dep] !== 'undefined') {
                                dep = typeof preset[dep] === 'string' ? preset[dep].split(' ') : preset[dep];
                                for (i = 0; i < dep.length; i++) {
                                    ret = ret.concat(check(dep[i]));
                                }
                            } else {
                                ret.push(dep);
                            }
                        }
                    }
                    // check array
                    else {
                        for (i = 0; i < dep.length; i++) {
                            ret = ret.concat(check(dep[i]));
                        }
                    }

                    return ret;
                };
        // check for overloaded params
        for (i = 1; i < arguments.length; i++) {
            arg = arguments[i];
            el = typeof arg === 'string' || typeof arg === 'object' ? arg : el;
            async = typeof arg === 'boolean' ? arg : async;
            callback = typeof arg === 'function' ? arg : callback;
        }

        // check if we should load the script
        if ((!el || $(el).length) && deps) {
            urls = check(deps);	// check against presets

            if (urls && urls.length > 0) {
                loadjs(urls, {
                    async: async,
                    success: function () {
                        if (typeof callback === 'function')
                            callback();
                    },
                    error: function (d) {
                        console.log('failed to load:');
                        console.log(d);
                    }
                });
            }
        }
    };
})();