function chooseShow(show) {
    if (show) {
        if (ccu.mquery !== 'mobile' && ccu.mquery !== 'tablet')
            $('.choose-title, .choose-list').removeClass('inactive')
                    .addClass('active').attr('aria-expanded', true);
    } else {
        $('.choose-title, .choose-list').removeClass('active')
                .addClass('inactive').attr('aria-expanded', false);
    }
}

$(function () {

    // coutup animations
    $('.countup').each(function (i) {
        // reset count
        var num = parseInt($(this).text()),
                del = parseInt($(this).attr('data-count-delay') ? $(this).attr('data-count-delay') : 0);
        //$(this).text('0');

        // set the countup to happen when scrolled into view
        $(this).scrollClass({
            delay: del,
            threshold: 50,
            callback: function (el) {
                var opts = {
                    useEasing: true
                },
                        dur = $(this).attr('data-count-duration') ? $(this).attr('data-count-duration') : 3,
                        cup = new CountUp(this, 0, num, 0, dur, opts).start();
            }
        });
    });

//    chooseShow(true);
});