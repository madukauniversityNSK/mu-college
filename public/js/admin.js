
/* Set the width of the side navigation to 250px and the left margin of 
 * the page content to 250px and add a black background color to body */
function openNav() {
    document.getElementById("sidenav").style.width = "250px";
    document.body.style.marginLeft = "250px";
    document.body.style.marginRight = "-250px";
    document.querySelector('.navbar-fixed-bottom, .navbar-fixed-top').style.left = "250px";
    document.querySelector('.navbar-fixed-bottom, .navbar-fixed-top').style.right = "-250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

/* Set the width of the side navigation to 0 and the left margin of the 
 * page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("sidenav").style.width = "0";
    document.body.style.marginLeft = "0";
    document.body.style.marginRight = "0";
    document.querySelector('.navbar-fixed-bottom, .navbar-fixed-top').style.left = "0";
    document.querySelector('.navbar-fixed-bottom, .navbar-fixed-top').style.right = "0";
    document.body.style.backgroundColor = "white";
}

$('#sidenav a.toggle-btn').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('open');
});