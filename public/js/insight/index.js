$(function () {
    minimize();

    $('.delete.icon').click(function (e) {
        e.preventDefault(); 
        var btn = this;
        var deleteurl = ($(btn).attr('deleteurl'));
        showAlertModal({
            text: 'Are you sure you want to delete this form?',
            type: 'question',
            okText: 'Yes, Continue',
            cancelText: 'Cancel',
            showCancel: true,
            okAction: function () {
                deletePoll(btn, deleteurl);
            }
        });

        function deletePoll(btn, deleteurl) {
            showPageLoader();
            ajaxCall({
                url: deleteurl,
                method: 'POST',
                onSuccess: function (xhr) {
                    if (xhr.status) {
                        $(btn).parents('.poll').remove();
                    } else {
                        notify($('p.notify'), xhr);
                    }
                },
                onFailure: function (xhr) {
                    handleHttpErrors(xhr, null, $('p.notify'));
                },
                onComplete: function () {
                    hidePageLoader();
                }
            });
        }
    });
});


