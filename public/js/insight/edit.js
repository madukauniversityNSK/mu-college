/**
 * Poll question object
 * @param {type} question
 * @returns {PollQuestion}
 */
function PollQuestion(question) {
    var $this = this;
    $this.exists = typeof (question) !== 'undefined';
    $this.dirty = false;
    $this.question = $this.exists ? {id: 0} : question;
    $this.parentForm = $('#sample_question');
    var form = $this.exists ? $this.parentForm.clone(true) : $this.parentForm;
    $this.form = form;
    $this.fields = {
        id: form.find('[name=id]'),
        content: form.find('[name=content]'),
        description: form.find('[name=description]'),
        poll_question_type_id: form.find('[name=poll_question_type_id]'),
        required: form.find('[name=required]'),
        'option[]': form.find("[name='option[]']"),
        /*numeric: form.find('[name=numeric]'),*/
        regex: form.find('[name=regex]'),
        min: form.find('[name=min]'),
        max: form.find('[name=max]'),
        minlength: form.find('[name=minlength]'),
        maxlength: form.find('[name=maxlength]')
    };
    $this.findFields = function () {
        return $this.fields;
    };

    //1=>radio, 4=>checkbox, 8=>rating, 9=>select-single, 10=>select-multiple
    $this.multichoice = [1, 4, 8, 9, 10];
    $this.contentType = [2, 3, 5];

    $this.data = function () {
        var data = {};
        for (var key in $this.fields) {
            switch (key) {
                // case 'content':
                //     data[key] = $this.fields[key].text();
                //     break;
                default:
                    data[key] = $this.fields[key].val();
            }
        }
        return data;
    };

    //This is used when a new question is added
    $('.btn-add', $this.form).off('click');//put off previous click event
    $('.btn-add', $this.form).on('click', function (e) {//assign a new event
        e.preventDefault();
        $this.addOption();
    });

    /*
     for selecting question type
     On selecting a question type, the question is switched temporary
     to the selected type, until update button is click, then it will be
     permanently saved(updated).
     */
    $(form.find('[name=poll_question_type_id]')).on('change selected', $this.form, function () {
        var qt_id = form.find('[name=poll_question_type_id]').val();
        $('.question_types_div').attr('id', 'qtd_' + qt_id);

        //get the question type description
        function questionTypeDescription() {
            var title = $('option:selected', $this.form).attr('title');
            var description = '<small class="grey-text" style="font-size: 14px">' + title + '</small>';
            return description;
        }

        var select = $('option:selected', $this.form).attr('value');


        var id = $('#qtd_' + qt_id).attr('id');
        //get the question type id
        var question_type_id = $this.form.attr('question-type-id');

        //  during updating, whenever a question-type is changed for a particular question
        //  fill-up/display the options (if one exist) for editing.
        $this.fillOptions = function () {
            $this.options = question.options;
            $this.options.forEach(function (element) {
                var content = element.content;
                var option_id = element.id;
                $this.addOptionWithInput(content, option_id);
            });

            //new form elements has been added,
            // let this class be aware of it
            $this.prepareForm();
        };

        //for replacing option block (radio-box and checkbox) div/element
        function option() {
            $this.option_div
                    = '<div class="col-md-12 question_types_div">' +
                    '<div class="row">' +
                    '<div id="option" class="options">' +
                    '</div>' +
                    '</div>' +
                    '<div class="new-cloned-btn">' +
                    '</div>' +
                    questionTypeDescription() +
                    '</div>';
            return $this.option_div;
        }

        //for editing questions only, when changed to radio, check box and
        //rating question-type, replace this div for the said question
        function addOptionbtn() {
            $('div.new-cloned-btn', $this.form).replaceWith('<a href="#" class="cloned-btn-add">Add option</a><br/>');
            $this.prepareForm();
        }

        // if the question is a content question; (users can type/enter their answers),
        // allow the form creator to specify min, max character for a particular answer.
        function content() {
            $this.content_div = '<div class="question_types_div">' +
                    '<div class="row padding-1em">' +
                    '<div class="form-group col-md-6 col-sm-6">' +
                    '<input placeholder="Minimum value" id="min" name="min" type="text" value="' +
                    (question.min === null ? '' : question.min) +
                    '" required class="validate form-control">' +
                    '<label for="min">Minimum value</label>' +
                    '</div>' +
                    '<div class="form-group col-md-6 col-sm-6">' +
                    '<input placeholder="Maximum" id="max" name="max" type="text" value="' +
                    (question.max === null ? '' : question.max) +
                    '" required class="validate form-control">' +
                    '<label for="max">Maximum value</label>' +
                    '</div>' +
                    '</div>' +
                    questionTypeDescription() +
                    '</div>';
            return $this.content_div;
        }

        //for text and textarea (short and long text) questions, change the following
        function labelText() {
            $('label[for=min]', $this.form).text('Minimum number of characters');
            $('label[for=max]', $this.form).text('Maximum number of characters');
            var type = $('option:selected', $this.form).attr('id');
            //min value/number to be entered must be >= int 1,
            // we cannot accept negative number of characters
            $('input[id=min]', $this.form).prop('min', '1').prop('type', 'number').prop('required', false);
            $('input[id=max]', $this.form).prop('min', '2').prop('type', 'number').prop('required', false);
            $('input[id=min]', $this.form).attr('placeholder', 'Optional');
            $('input[id=max]', $this.form).attr('placeholder', 'Optional');
        }


        switch (qt_id) {
            //radio box
            case '1':
                $('#qtd_' + qt_id, $this.form).replaceWith(option());
                //this is an option question
                if ($.inArray(question_type_id, $this.multichoice) > -1) {
                    $this.fillOptions();
                } else {
                    //it is not
                    addOptionbtn();
                }
                break;
            case '2':
                //short text ... text input
                $('#qtd_' + qt_id, $this.form).replaceWith(content());
                labelText();
                break;
            case '3':
                //long text ... textarea
                $('#qtd_' + qt_id, $this.form).replaceWith(content());
                labelText();
                break;
            case '4':
                //check box
                $('#qtd_' + qt_id, $this.form).replaceWith(option());
                if ($.inArray(question_type_id, $this.multichoice) > -1) {
                    $this.fillOptions();
                } else {
                    addOptionbtn();
                }
                break;
            case '5':
                //range
                $('#qtd_' + qt_id, $this.form).replaceWith(content());
                break;
            case '6':
                //date
                $('#qtd_' + qt_id, $this.form).replaceWith(
                        '<div class="col-md-12 question_types_div">' +
                        questionTypeDescription() +
                        '</div>'
                        );
                break;
            case '7':
                //time
                $('#qtd_' + qt_id, $this.form).replaceWith(
                        '<div class="col-md-12 question_types_div">' +
                        questionTypeDescription() +
                        '</div>');
                break;
            case '8':
                //rating
                $('#qtd_' + qt_id, $this.form).replaceWith(option());
                if ($.inArray(question_type_id, $this.multichoice) > -1) {
                    $this.fillOptions();
                } else {
                    addOptionbtn();
                }
                break;
            case '9':
                //select-single
                $('#qtd_' + qt_id, $this.form).replaceWith(option());
                if ($.inArray(question_type_id, $this.multichoice) > -1) {
                    $this.fillOptions();
                } else {
                    addOptionbtn();
                }
                break;
            case '10':
                //select-multiple
                $('#qtd_' + qt_id, $this.form).replaceWith(option());
                if ($.inArray(question_type_id, $this.multichoice) > -1) {
                    $this.fillOptions();
                } else {
                    addOptionbtn();
                }
                break;
            default:

        }
    });

    $this.updateFields = function (question) {
        $this.question = question;

        var q_id = 'question-' + $this.question.id;
        $this.form.attr('id', q_id);
        //every form should have a
        //'question-type-id attribute', this is used
        //to know the question type of a particular question
        $this.form.attr('question-type-id', $this.question.poll_question_type_id);
        //Update form fields

        for (var key in $this.fields) {
            if (key in  $this.question) {
                $this.fields[key].val($this.question[key]);
            }
        }

        //Update ids
        for (var field in $this.fields) {
            var old_id = $this.fields[field].attr('id');
            var uid = $this.question.id + '-' + old_id;
            $this.fields[field].attr('id', uid);
            $("label[for='" + old_id + "']", $this.form).attr('for', uid);
        }

        $this.dirty = false;
    };

    $this.sync = function () {
        $('button[type=submit]', $this.form).prop('disabled', true);

        //Validate input
        // Get the question type
        var select_option = ($('option:selected', $this.form).attr('value'));

        if (select_option == 5) {
            //validation of range values begins
            var error;
            // Get the value of the input field with min, max name
            var min = $('input[name=min]', $this.form).val();
            var max = $('input[name=max]', $this.form).val();
            // If min, max is Not a Number
            if ((isNaN(min) || isNaN(max))) {
                $('button[type=submit]', $this.form).prop('disabled', false);
                error = "Minimum/Maximum value must be a number";
                toast(error, 8000);
                return false;
            }
            //if min greater than max, or max is less than min, max is equal to min
            if ((parseFloat(min) > parseFloat(max)) || (parseFloat(max) === parseFloat(min))) {
                $('button[type=submit]', $this.form).prop('disabled', false);
                error = "Maximum value must be greater than minimum value";
                toast(error, 8000);
                return false;
            }//validation of range values ends

        }

        //Make ajax request to update question
        ajaxCall({
            method: 'POST',
            url: update_question_url,
            data: $this.form.serialize(),
            onSuccess: function (xhr) {
                if (xhr.status) {

                    rebuildOptions(xhr.options);

                    //Update fields
                    $this.updateFields(xhr.data);
                    $this.prepareForm();
                    notify($('p.notify', $this.form), xhr);

                } else {
                    //Handle validation error
                    handleHttpErrors(xhr, $this.form);
                }
            },
            onFailure: function (xhr) {
                handleHttpErrors(xhr, $this.form);
            },
            onComplete: function (xhr) {
                $('button[type=submit]', $this.form).prop('disabled', false);
            }
        });
    };

    /**
     * Rebuild a question options on-update
     * @param arrayOptions
     * @returns {string|*}
     */
    function rebuildOptions(arrayOptions) {
        $('.options', $this.form).children().remove();
        var options = arrayOptions;
        $this.row = '';
        for (var index in options) {
            //button for removing text-box
            $this.rem_option_btn = '<a title="Remove this option" class="btn_remove col-sm-1 col-md-11 font-bold"><span style="cursor: pointer;">X</span></a>';
            $this.row += $('.options', $this.form)
                    .append('<div class="form-group"><div class="col-md-11"><input class="opt-input-group form-control" value="' + options[index] + '" required name="option[' + (index) + ']" type="text"/></div>' + $this.rem_option_btn + '</div>');
        }
        return $this.row;
    }

    $this.prepareForm = function () {
        if ($this.exists) {
            //Give the current form an id attribute
            $this.form.attr('id', question.id);
            //check if this is a multi-choice question
            if ($.inArray(question.poll_question_type_id, $this.multichoice) > -1) {
                if (question.answers.length > 0) {
                    //This question has been answered, then indicate on the html form
                    $this.form.attr('data-question-answered', question.id);
                }
            }

            $('button[type=submit]', $this.form).text('Update').removeClass('mdi mdi-plus');
            //remove the class 'create', it for creating a new question
            $('#create', $this.form).removeClass('create');
            var type_id = question.poll_question_type_id;
            //check if the question is an option/multi-choice questions
            // if (type_id === 1 || type_id === 4 || type_id === 8) {

            //Off any-'previous', click event that were attached
            $('.cloned-btn-add', $this.form).off('click');
            //replace the original btn, with this new one
            $('a.btn-add', $this.form).replaceWith('<a href="#" class="cloned-btn-add">Add option</a>');
            // }

            //on-changing question-types, check if there is any delete button,
            if ($('.delete-question', $this.form).length === 0) {
                //append a new delete button to the specified class.
                //this happens on page reload/ready/(on class clone)
                $('.delete-btn', $this.form).append(
                        '<a class="delete-question icon" style="font-size: 30px; cursor: pointer;"><i class="mdi mdi-delete" title="Delete this question"></i></a>'
                        );
            }

            $($this.form).off('submit');
            $this.form.submit(function (e) {
                e.preventDefault();

                var answered = $this.form.attr('data-question-answered');
                if (answered.length > 0 && parseInt(answered) === question.id) {
                    //for multi-choice only

                    var message = 'Responses already exist for this question\n' +
                            'Are you sure you want to update this question?\n' +
                            'Updating this question may delete its previous responses:\n' +
                            'Click:\n\n' +
                            'Ok: To update the question\n' +
                            'Cancel: To abort';
                    var result = confirm(message);

                    if (result) {
                        $this.sync();
                    }

                    return false;
                } else {
                    //Go ahead and update the said question, for content question
                    $this.sync();//and sync back to blade
                }
                return false;
            });
        } else {
            $('button[type=submit]', $this.form).text('Add');
            $($this.form).off('submit');
            $this.form.submit(function (e) {
                e.preventDefault();
                //$this.createNew(); //This is not used
                return false;
            });
        }

        //Monitor changes to this form
        $('input, select, textarea', $this.form).on('keypress change', function () {
            $this.dirty = true;
        });

        //On clicking this current form,
        $('.cloned-btn-add', $this.form).click(function (e) {
            e.preventDefault();
            $this.addOption();
        });

        //removing option text box
        $(document).on('click', '.btn_remove', $this.form, function (e) {
            // Do something on an existent or future .dynamicElement
            e.preventDefault();

            $(this).parent().remove();

        });

        //set a question to required, compulsory
        $('.required', $this.form).on('change click', function () {
            console.log($(this).attr('poll_id'));
            ajaxCall({
                url: $(this).attr('url') + '/' + question.id + '/' + $(this).attr('poll_id'),
                method: 'POST',
                data: $(this).serialize(),
                onSuccess: function (xhr) {
                    toast('Success', 3000);
                },
                onFailure: function (xhr) {
                    handleHttpErrors(xhr, $this.form, false);
                    toast('Setting question to required failed', 3000);
                },
                onComplete: function () {
                    hidePageLoader();
                }
            });
        });

    };

    //button for removing text-box
    $this.rem_option_btn = '<a title="Remove this option" class="btn_remove col-md-1 col-sm-11 font-bold"><span style="cursor: pointer;">X</span></a>';
    //for adding new text-box. Used for Adding new options into an existing questions
    $this.addOption = function () {
        $('.options', $this.form).append('<div class="form-group padding-1em"><div class="col-md-11 col-sm-11"><input type="text" class="opt-input-group form-control" required id="option" name="option[]" placeholder="New option"/></div>' + $this.rem_option_btn + '</div>');
    };

    //On changing question type change options->content
    $this.addOptionWithInput = function (content, option_id) {
        $('.options', $this.form).append('<div class="form-group padding-1em"><div class="col-md-11 col-sm-11"><input type="text" class="opt-input-group form-control" required value="' + content + '" name="option[' + (option_id) + ']" placeholder="New option"/></div>' + $this.rem_option_btn + '</div>');
    };

    /*
     various options for each question
     Create an input text-box for options of "this" form/question
     and populate the input with the options values(content).
     */
    $this.updateOptionInputs = function (question) {
        $this.options = question.options;
        $this.questionTypeID = question.poll_question_type_id;
        for (var index in $this.options) {
            if ($.inArray($this.questionTypeID, $this.multichoice) > -1) {
                $this.question.options = $this.options[index].content;
                $this.question.options.id = $this.options[index].id;
                $('.options', $this.form).append('<div class="form-group padding-1em"><div class="col-md-11 col-sm-11"><input class="opt-input-group form-control" value="' + $this.question.options + '" required name="option[' + ($this.options[index].id) + ']" type="text"/></div>' + $this.rem_option_btn + '</div>');
            }
        }

    };


    $this.updateContentInputs = function () {
        var type_id = question.poll_question_type_id;
        if ($.inArray(type_id, $this.contentType) > -1) {
            //it is content type of question
            //if the question is a content question; (users can type/enter their answers),
            // allow the form creator to specify min, max character for a particular answer.
            function content() {
                $this.content_div = '<div class="question_types_div">' +
                        '<div class="row padding-1em">' +
                        '<div class="form-group  col-md-6 col-sm-6">' +
                        '<input id="min" name="min" type="' + (type_id === 5 ? 'text' : 'number') + '" value="' + (question.min === null ? '' : question.min) + '" required class="validate form-control">' +
                        '</div>' +
                        '<div class="form-group col-md-6 col-sm-6">' +
                        '<input id="max" name="max" type="' + (type_id === 5 ? 'text' : 'number') + '" value="' + (question.max === null ? '' : question.max) + '" required class="validate form-control">' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                return $this.content_div;
            }

            $('.question_types_div').attr('id', 'qtd_' + type_id);
            $('.question_types_div', $this.form).replaceWith(content());

        }

    };

    /**
     * check a question "required" status
     * then set the question's "required switch", to the status so obtain
     */
    $this.required = function () {
        var required = (question.required === 0 ? '' : 'checked');
        $('.required', $this.form).prop('checked', required);
    };

    /**
     * This deletes a question,
     */
    $this.deleteQuestion = function () {
        $('.delete-question', $this.form).click(function (e) {
            e.preventDefault();
            $('button[type=submit]', $this.form).prop('disabled', true);
            var question_id = $('input[name=id]', $this.form).attr('value');
            showAlertModal({
                text: 'Are you sure you want to delete this question?',
                type: 'question',
                okText: 'Yes, Delete',
                cancelText: 'Cancel',
                showCancel: true,
                okAction: function () {
                    $($this.form).slideUp('slow');
                    deleteQuestion();
                }
            });

            //deletes the selected question
            function deleteQuestion() {
                ajaxCall({
                    url: delete_question_url + '/' + question_id,
                    method: 'POST',
                    onSuccess: function (xhr) {
                        if (xhr.status) {
                            $this.form.remove();
                            toast('Question deleted successfully!', 3000);
                        } else {
                            notify($('p.notify'), xhr);
                            $($this.form).slideDown('slow');
                            toast('Error occurred during deleting', 3000);
                        }
                    },
                    onFailure: function (xhr) {
                        handleHttpErrors(xhr, $this.form, $('p.notify', $this.form), false);
                        $($this.form).slideDown('slow');
                        hidePageLoader();
                    },
                    onComplete: function () {
                        hidePageLoader();
                        $('button[type=submit]', $this.form).prop('disabled', false);
                    }
                });
            }
        });
    };

    $this.updateContentInputs();
    $this.updateOptionInputs(question);
    $this.prepareForm();
    $this.updateFields(question);
    $this.deleteQuestion();
    $this.required();
    return $this;
}//end of object PollQuestion(question)


var questionList = [];
$(function () {
    /*Updating questions view*/
    for (var index in questions) {
        var pollQuestion = new PollQuestion(questions[index]);
        addQuestionToView(pollQuestion);
    }

    /*Update materialize select*/
    $('select').select();
});

function addQuestionToView(pollQuestion) {
    questionList[pollQuestion.fields.id] = pollQuestion;
    $('#questions').append(pollQuestion.form.removeClass('z-depth-1'));
}

$('.delete.icon').click(function (e) {
    e.preventDefault();

    showAlertModal({
        text: 'Are you sure you want to delete this form?',
        type: 'question',
        okText: 'Yes, Continue',
        cancelText: 'Cancel',
        showCancel: true,
        okAction: deletePoll
    });

    function deletePoll() {
        showPageLoader();
        ajaxCall({
            url: deleteurl,
            method: 'POST',
            onSuccess: function (xhr) {
                if (xhr.status) {
                    window.location = formurl;
                } else {
                    notify($('p.notify'), xhr);
                }
            },
            onFailure: function (xhr) {
                handleHttpErrors(xhr, null, $('p.notify'));
            },
            onComplete: function () {
                hidePageLoader();
            }
        });
    }
});


/**
 * This for manipulating original/static form, not the clone forms
 */

//Adding/Creating new question
$('#sample_question').submit(function (e) {
    e.preventDefault();
    // Get the question type
    var select_option = ($('#poll_question_type_id').val());

    if (select_option == 5) {
        //validation of range values begins
        var error;
        // Get the value of the input field with min, max name
        var min = $('input[name=min]', $form).val();
        var max = $('input[name=max]', $form).val();
        // If min, max is Not a Number
        if ((isNaN(min) || isNaN(max))) {
            error = "Minimum/Maximum value must be a number";
            toast(error, 8000);
            return false;
        }
        //if min greater than max, or max is less than min, max is equal to min
        if ((parseFloat(min) > parseFloat(max)) || (parseFloat(max) === parseFloat(min))) {
            error = "Maximum value must be greater than minimum value";
            toast(error, 8000);
            return false;
        }//validation of range values ends
    }

    var $this = this;
    showPageLoader();
    ajaxCall({
        type: 'POST',
        url: create_question_url,
        data: $($this).serialize(),
        onSuccess: function (xhr) {
            if (xhr.status) {

                $('.options', '#sample_question').children().remove();
                var pollQuestion = new PollQuestion(xhr.data);
                $('select').select();
                $('#questions').append(pollQuestion.form.removeClass('z-depth-1'));

                //Rebuild a question options
                var options = xhr.options;
                for (var index in options) {
                    //button for removing text-box
                    $this.rem_option_btn = '<a title="Remove this option" class="btn_remove col-md-1 col-sm-1 font-bold"><span style="cursor: pointer;">X</span></a>';
                    $('.options', $this.form).append('<div class="form-group"><div class="col-md-11 col-sm-11"><input class="opt-input-group form-control" value="' + options[index] + '" required name="option[' + (index) + ']" type="text"/></div>' + $this.rem_option_btn + '</div>');
                }
                //End Rebuild a question options

                $('.options', '#sample_question').children().remove();
                $('select').select();
                document.getElementById("sample_question").reset();
                //On Adding question, retain previous selected value as current/selected
                $("#poll_question_type_id option[value=" + select_option + "]").prop('selected', true);
                toast('Question created successfully!', 5000);

            } else {
                handleHttpErrors(xhr, $form);
            }
        },
        onFailure: function (xhr) {
            hidePageLoader();
            handleHttpErrors(xhr, $form);
            //notify($('p.notify', '#sample_question'), xhr);
            toast('Error occurred', 8000);
        },
        onComplete: function (xhr) {
            hidePageLoader();
            $('button[type=submit]', $form).prop('disabled', false);


        }

    });
});

//for changing question types
var $form = $('#sample_question');
$('select[name=poll_question_type_id]', $form).on('change selected', function () {
    var $this = this;
    //get the value of the form element it is == question-type-id
    var question_type_id = $('option:selected', $form).attr('value');

    //set the question-type-id to be the 'id' of the selected class (question_types_div)
    $('.question_types_div').attr('id', 'qtd_' + question_type_id);

    //get the question type description
    function questionTypeDescription() {
        var title = $('option:selected', $form).attr('title');
        var description = '<small class="grey-text" style="font-size: 14px">' + title + '</small>';
        return description;
    }

    //for replacing option block (radio-box and checkbox) div/element
    function option() {
        $this.option_div
                = '<div class="col-md-12 col-sm-12 question_types_div">' +
                '<div class="row">' +
                '<div id="option" class="options">' +
                '</div>' +
                '</div>' +
                '<div class="new-cloned-btn">' +
                '</div>' +
                '<a href="#" id="add-option" class="btn-add">Add option</a><br/>' +
                questionTypeDescription() +
                '</div>';
        return $this.option_div;
    }
    ;

    // if the question is a content question; (users can type/enter their answers),
    // allow the form creator to specify min, max character for a particular answer.
    //for text and textarea
    function content() {
        $this.content_div = '<div class="col-md-12 col-sm-12 question_types_div">' +
                '<div class="row">' +
                '<div class="input-field col-md-6 col-sm-6">' +
                '<label for="min">Minimum value</label>' +
                '<input id="min" name="min" type="text" value="" required class="form-control validate"/>' +
                '</div>' +
                '<div class="input-field col-md-6 col-sm-6">' +
                '<label for="max">Maximum value</label>' +
                '<input id="max" name="max" type="text" value="" required class="form-control validate"/>' +
                '</div>' +
                '</div>' +
                '<br/>' + questionTypeDescription() +
                '</div>';
        return $this.content_div;
    }

    //for text and textarea (short and long text) questions, change the following
    function labelText() {
        $('label[for=min]', $form).text('Minimum number of characters');
        $('label[for=max]', $form).text('Maximum number of characters');
        var type = $('option:selected', $form).attr('id');
        //min value/number to be entered must be >= int 1,
        // we cannot accept negative number of characters
        $('input[id=min]', $this.form).prop('min', '1').prop('type', 'number').prop('required', false);
        $('input[id=max]', $this.form).prop('min', '2').prop('type', 'number').prop('required', false);
        $('input[id=min]', $this.form).addClass('form-control').attr('placeholder', 'Optional');
        $('input[id=max]', $this.form).addClass('form-control').attr('placeholder', 'Optional');
    }


    switch (question_type_id) {
        //radio box
        case '1':
            $('#qtd_' + question_type_id, $form).replaceWith(option());
            prepareOptionQuestion();
            break;
        case '2':
            //short text ... text input
            $('#qtd_' + question_type_id, $form).replaceWith(content());
            labelText();
            break;
        case '3':
            //long text ... textarea
            $('#qtd_' + question_type_id, $form).replaceWith(content());
            labelText();
            break;
        case '4':
            //check box
            $('#qtd_' + question_type_id, $form).replaceWith(option());
            prepareOptionQuestion();
            break;
        case '5':
            //range
            $('#qtd_' + question_type_id, $form).replaceWith(content());

            break;
        case '6':
            //date
            $('#qtd_' + question_type_id, $form).replaceWith(
                    '<div class="col-md-12 col-sm-12 question_types_div">' +
                    questionTypeDescription() +
                    '</div>'
                    );
            break;
        case '7':
            //time
            $('#qtd_' + question_type_id, $form).replaceWith(
                    '<div class="col-md-12 col-sm-12 question_types_div">' +
                    questionTypeDescription() +
                    '</div>');
            break;
        case '8':
            //raing
            $('#qtd_' + question_type_id, $form).replaceWith(option());
            prepareOptionQuestion();
            break;
        default:

    }
});


function prepareOptionQuestion() {
    //On clicking this current/original (not the cloned form,
    function textInputOption() {
        //for removing an input/option text box
        var rem_option_btn = '<a href="#" title="Remove this option" class="btn btn_remove col-md-1 col-sm-1">X</a>';
        //text box for collecting option
        var text_input_option = '<div class="col-md-11 col-sm-11"><input type="text" id="option" class="form-control" required validate name="option[]" placeholder="New option"/></div>';
        $('.options', '#sample_question').append('<div>' + text_input_option + rem_option_btn + '</div>');
    }

    $('.btn-add', $form).click(function (e) {
        e.preventDefault();
        textInputOption();
    });

//for removing added input options
    //$('.btn_remove').on('click', '#sample_question', function(e) {//this is not working if no initial question. i.e the event is not tied yet
    $(document).on('click', '.btn_remove', $form, function (e) {
        // Do something on an existing or future .dynamicElement
        e.preventDefault();
        //the added text input is a sibling of .btn_removing class (remove text input)
        $(this).parent().remove();
    });
}

prepareOptionQuestion();


/*Publish a form, accept response, update expiry message*/
$(document).ready(function () {
    //Minimze
    minimize();
    /*update message button*/
    var btn_msg = $('.btn_close_message');
    $('form[class=published_accept_response]').on('change checked submit', function (e) {
        e.preventDefault();

        var url = $(this).attr('url');
        var form_id = $(this).attr('id');
        var element_state;
        var element_input = $(this).serialize();

        if (form_id === 'published_form') {
            var publish_id = 'published';
            var inputs = 'This action cannot be reversed, however you can turn off responses afterwards';

            showAlertModal({
                title: 'Are you sure?',
                text: inputs,
                okText: 'Yes, Publish',
                cancelText: 'Cancel',
                showCancel: true,
                okAction: function () {
                    element_state = $("#published").prop('disabled', true);
                    $('#' + publish_id).text('Published!');
                    adjustRaadaaForm(url, publish_id, element_state, element_input);
                    window.location.reload();
                }
            });
        } else if (form_id === 'accept_response_form') {
            if ($("#accept_response").prop('checked') === true) {
                $("#message").hide();
                element_state = $("#accept_response").prop('checked');
                var accept_id = 'accept_response';
                btn_msg.hide();
                adjustRaadaaForm(url, accept_id, element_state, element_input);
            } else if ($("#accept_response").prop('checked') === false) {
                $("#message").show();
                element_state = $("#accept_response").prop('checked');
                var accept_id = 'accept_response';
                btn_msg.show();
                adjustRaadaaForm(url, accept_id, element_state, element_input);
            }
        } else if (form_id === 'close_message') {
            element_state = $("#btn_message").prop('disabled', true);
            var btn_id = 'btn_message';
            adjustRaadaaForm(url, btn_id, element_state, element_input);
        }

    });

    /**
     * Make changes to raadaa form:
     * @Param:
     * @fields: [that may],[to] be adjusted: (publish, accept response, close_message)
     * Submits the changes
     * @return: A response to the form that make the call
     */
    function adjustRaadaaForm(url, id, state, input) {
        showPageLoader();
        ajaxCall({
            type: 'POST',
            url: url,
            data: input,
            onSuccess: function (xhr) {
                hidePageLoader();
                if (xhr.status) {
                    notify($('p.notify_form'), xhr);
                    if (($('#pubdate').val()).length !== 0 && ($('#pubtime').val()).length !== 0) {
                        schedulePublish();
                    }
                } else {
                    notify($('p.notify_form'), xhr);
                    if (id === 'published') {
                        $("#" + id).prop('disabled', false).text('Publish');
                    }
                }
            },
            onFailure: function (xhr) {
                handleHttpErrors(xhr);
                hidePageLoader();
                if (id === 'published') {
                    $("#" + id).prop('disabled', false);
                } else if (id === 'btn_message') {
                    /*for button*/
                    $("#" + id).prop('disabled', false);
                } else if (id === 'message') {
                    /*$(".close_message").text(state)*/
                } else if (id === 'accept_response') {
                    if (state === 'checked') {
                        $("#accept_response").prop("checked", true);
                    } else {
                        $("#accept_response").prop("checked", false);
                        $("#message").show();
                    }
                }
            },
            onComplete: function (xhr) {
                if (id === 'btn_message') {
                    $("#" + id).prop('disabled', false);
                }
            }
        });
    }


    /**
     * Send request for form to be publish at later date/time
     */
    function schedulePublish() {
        ajaxCall({
            type: 'POST',
            url: schedule_publish,
            data: $('#schedule-pub-date').serialize(),
            onSuccess: function (xhr) {
                if (xhr.status) {
                    notify($('p.notify_form'), xhr);
                } else {
                    notify($('p.notify_form'), xhr);
                }
            },
            onFailure: function (xhr) {
                handleHttpErrors(xhr);
            },
            onComplete: function (xhr) {

            }
        });
    }

});


//***********************************
//Short code
//*****************************************
var short_code_tio;
var short_code;
$('#short_code').keyup(function () {
    var el = this;
    el.style.background_color = 'white';
    if (short_code === el.innerText) {
        return;
    }
    short_code = el.innerText;

    if (short_code_tio) {
        clearTimeout(short_code_tio);
    }

    short_code_tio = setTimeout(function () {
        ajaxCall({
            url: short_code_url,
            data: {'short_code': short_code},
            onSuccess: function (xhr) {
                handleHttpErrors(xhr, null, '#short_code ~ p.notify');
                $('#short_code ~ p.notify').append('<br/>Refresh page to fully apply changes.');
                $(el).next('button')
                        .removeAttr('onclick')
                        .off('click')
                        .on('click', function () {
                            copyToClipboard(poll_view_url + '/' + xhr.short_code);
                        });
            },
            onFailure: function (xhr) {
                el.style.background_color = 'pink';
                handleHttpErrors(xhr, null, '#short_code ~ p.notify');
            }
        });
    }, 2000);
});

$('.make_public').on('click', function (e) {

    ajaxCall({
        type: 'POST',
        url: $(this).attr('url'),
        data: $('.make_public').serialize(),
        onSuccess: function (xhr) {
            if (xhr.status) {
                notify($('p.notify_form'), xhr);
            } else {
                notify($('p.notify_form'), xhr);
            }
        },
        onFailure: function (xhr) {
            handleHttpErrors(xhr);
        },
        onComplete: function (xhr) {

        }
    });
});

