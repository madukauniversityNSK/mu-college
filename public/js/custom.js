
function openNav() {
    if (!$('#navOverlay').length) {
        $('body').append($('<div>').attr('id', 'navOverlay').on('click', closeNav));
    }

    $('#navOverlay').show();
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    $('#navOverlay').hide();
    document.getElementById("mySidenav").style.width = "0";
}
