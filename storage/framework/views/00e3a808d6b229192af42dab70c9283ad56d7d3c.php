<?php $__env->startSection('title', 'Staff'); ?>

<?php $__env->startSection('content'); ?>
<hr style="margin-top: -5px">
<div class=" py-4">
    <h2 class="mb-4 text-center">Our Staffs</h2>
<div>
<section>
    <div class="container py-4">
        <div class="row mb-4">
            <?php $__currentLoopData = $staff; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4"style="margin-bottom:30px" >
                <div class="card">
                    <img style="height: 300px" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($p->filename); ?>" class="card-img-top" alt="...">
                    <div class="card-body" >
                        <div class="card-text" ><?php echo e($p->name); ?></div> 
                        <div class="blockquote-footer font-weight-bold"><?php echo e($p->description); ?></div>   
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <div>  
    </div>
</section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/teachers.blade.php ENDPATH**/ ?>