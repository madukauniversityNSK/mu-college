<!doctype html>
<html lang="en">
<head>
   
    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <meta name="robots" content="index, follow">

    <!-- FAVICONS ICON ============================================= -->

        <link rel="icon" href="<?php echo e(asset('/assets/front/images/avatars/'.config('website.logo'))); ?>" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('/assets/front/images/avatars/'.config('website.logo'))); ?>" />

    
    <!--====== Title ======-->
    <title><?php echo $__env->yieldContent('title'); ?></title>
    
    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="<?php echo e(asset('storage/logo/favicon.png')); ?>" type="image/png">
    <style>
       img {
            display: block;
            max-width:1400px;
            max-height:559px;
            width: auto;
            height: auto;
        }
        .link-color{
            color: white;
        }
        .link-color:hover{
            color: red;
        }
    </style>
    <?php echo $__env->make('inc.style', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldPushContent('css'); ?>
   

</head>

<body class="">
   <?php $__env->startSection('sidebar'); ?>
    <?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldSection(); ?>
    <main >
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
       
    </main>
    <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
     <?php echo $__env->yieldPushContent('scripts'); ?>
  <script>

 </script>
</body>
</html><?php /**PATH C:\xampp\htdocs\ixora\resources\views/layouts/frontend.blade.php ENDPATH**/ ?>