<!doctype html>
<html lang="en">
<head>
   
    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    
    <!--====== Title ======-->
    <title><?php echo $__env->yieldContent('title'); ?></title>
    
    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="<?php echo e(asset('storage/logo/favicon.png')); ?>" type="image/png">
    <?php echo $__env->make('backend.Partials.styles', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldPushContent('css'); ?>
  
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <?php echo $__env->make('backend.Partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
  
    <?php echo $__env->make('backend.Partials.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->yieldPushContent('scripts'); ?>
</body>
</html>

<body><?php /**PATH C:\xampp\htdocs\ixora\resources\views/layouts/backend.blade.php ENDPATH**/ ?>