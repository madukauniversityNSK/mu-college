<?php $__env->startSection('title', 'Gallery'); ?>

<?php $__env->startSection('content'); ?>
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left  text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Manage Gallery</h>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_tile">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Manage Galleries</a>
                        </li>
                        <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Create Gallery</a>
                        </li>
                    </ul>
                    <div class="tab-content py-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div>
                             <?php if(session()->has('message')): ?>
                               <div class="alert alert-success">
                                  <?php echo e(session('message')); ?>

                                </div>
                              <?php endif; ?>
                            </div>
                            <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                   
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card-box table-responsive">
                                                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Title</th>
                                                                <th>image</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php $__currentLoopData = $record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                               
                                                                <td ><?php echo e($records->title); ?></td>
                                                                <td class="text-center"><img height="100" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($records->filename); ?>" class="" alt="slider"></td>
                                                                <td>
                                                                    <div class="dropdown">
                                                                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fa fa-bars text-success"></i> 
                                                                        </a>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                                            <a class="dropdown-item" href="<?php echo e(route('edit-gallery', $records->id)); ?>"><i class="fa fa-pencil"></i>  Edit</a>
                                                                            <a class="dropdown-item" href="<?php echo e(route('delete-gallery', $records->id)); ?>"><i class="fa fa-remove"></i>  Delete</a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="x_content">
                                <form action="<?php echo e(route('save-gallery')); ?>" method='POST' class="py-4" enctype="multipart/form-data">  
                                         <?php echo csrf_field(); ?>
                                    <div class="row">
                                         <div class="col-sm-6">
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Title</label>  
                                                <input type="text" class=" form-control form-control <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="title" value="<?php echo e(old('title')); ?>"  autofocus required>  
                                                <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                     <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                             </fieldset>
                                             <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                                <input type="file" class=" form-control form-control <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="image" value=""  required>  
                                                <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </fieldset>  
                                            <div class="">
                                                <button class="btn btn-lg btn-primary float-right" type="submit"  >save</button>
                                            </div>
                                       </div>
                                    </div>
                               </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/backend/gallery/index.blade.php ENDPATH**/ ?>