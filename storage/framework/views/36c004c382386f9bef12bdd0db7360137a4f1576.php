<?php $__env->startSection('title', 'Login'); ?>

<?php $__env->startSection('content'); ?>

<section class="container-fluid bg-light" style="margin-top:50px; height:500px">
   <hr class=" d-none d-sm-block" style="margin-top: -40px">
    <div class="card mx-auto"  style="max-width: 30rem;">
        <div class="card-body">
             <?php if(session('status')): ?>
            <p class="alert alert-success" > <?php echo e(session('status')); ?> </p>
            <?php endif; ?>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <p class="text-danger"><?php echo e($error); ?></p>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <p class="h4 mb-4 text-dark" >Login</p>
            <form class="" action="<?php echo e(route('login')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <div class="form-group mb-4">
                    <label>Email</label>
                    <input class="form-control form-control-lg" type="email" name="email" required autofocus>
                </div>
                <div class="form-group mb-3">
                    <label>Password</label>
                    <input class="form-control form-control-lg" type="password" name="password" required autofocus>
                </div>
                <div class=" form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        remember me
                    </label>
                    <a class="float-end text-dark text-decoration-none" href="<?php echo e(route('password.request')); ?>" style="">Forgot password?</a>
                </div>
                <div class="d-grid gap-2"  style="margin-top:30px">
                    <button class="btn btn-lg btn-dark" type="submit">Login</button>
                </div>
            </form>
        </div>
    </div>
</section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/auth/login.blade.php ENDPATH**/ ?>