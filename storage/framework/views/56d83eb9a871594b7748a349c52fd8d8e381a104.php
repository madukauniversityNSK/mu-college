<?php $__env->startSection('title', 'Post Edit'); ?>

<?php $__env->startSection('content'); ?>
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Post Edit</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                        <form action="<?php echo e(route('update-event')); ?>" method='POST' class="py-4" enctype="multipart/form-data">  
                                         <?php echo csrf_field(); ?>
    
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Name</label>  
                                                <input type="text" class=" form-control form-control <?php if ($errors->has('event_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('event_name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="event_name" value="<?php echo e(old('event_name', $record->event_name)); ?>"  autofocus required>  
                                                <?php if ($errors->has('event_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('event_name'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                     <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Start Date</label>  
                                                <input type="datetime-local" class=" form-control form-control <?php if ($errors->has('start_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('start_date'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="start_date" value="<?php echo e(old('start_date', $record->start_date)); ?>" required>  
                                                <?php if ($errors->has('start_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('start_date'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">End Date</label>  
                                                <input type="datetime-local" class=" form-control form-control <?php if ($errors->has('end_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('end_date'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="end_date" value="<?php echo e(old('end_date', $record->end_date)); ?>" required >  
                                                <?php if ($errors->has('end_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('end_date'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </fieldset>
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Location</label>  
                                                <input type="text" class=" form-control form-control <?php if ($errors->has('location')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('location'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="location" value="<?php echo e(old('location', $record->location)); ?>" required>  
                                                <?php if ($errors->has('location')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('location'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </fieldset> 
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                                <input type="file" class=" form-control form-control <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="image" value="" >  
                                                <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </fieldset>
                                            <input type="hidden" class=" form-control form-control <?php if ($errors->has('id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('id'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="id" value="<?php echo e(old('id', $record->id)); ?>">  
                                            <fieldset class="form-group mb-3">   
                                                <label for="exampleFormControlInput1" class="form-label">Description</label>  
                                                <textarea name="event_description"  class="form-control form-control <?php if ($errors->has('event_description')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('event_description'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"  ><?php echo e(old('event_description', $record->event_description)); ?></textarea>
                                                    <?php if ($errors->has('event_description')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('event_description'); ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($message); ?></strong>
                                                </span>
                                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </fieldset>
                                            <div class="">
                                                <button class="btn btn-lg btn-primary float-right" type="submit"  >submit</button>
                                            </div>
    
                               </form>
                        </div>
                        <div class="col-sm-6">
                            <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($record->filename); ?>"  class="img-fluid" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->startPush('scripts'); ?>
    <script src="<?php echo e(asset('summernote/dist/summernote-lite.js')); ?>"></script>
    <script src="<?php echo e(asset('summernote/dist/lang/summernote-es-ES.js')); ?>"></script>
    <script>
    $('.summernote').summernote({

    });
    </script>
    
    <?php $__env->stopPush(); ?>
     
    <?php $__env->startPush('css'); ?>
      
    
    <link href="<?php echo e(asset('summernote/dist/summernote-lite.css')); ?>" rel="stylesheet"></link>
 
    <?php $__env->stopPush(); ?>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/backend/event/edit.blade.php ENDPATH**/ ?>