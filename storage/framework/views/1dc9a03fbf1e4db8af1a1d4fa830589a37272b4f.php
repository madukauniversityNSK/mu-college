<?php $__env->startSection('title', 'Home'); ?>


<?php $__env->startSection('content'); ?>

<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel" style="margin-bottom: 60px">
   <div class="carousel-inner">
         <?php $isFirst = true; ?>
         <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="carousel-item<?php echo e($isFirst ? ' active' : ''); ?>" data-interval="10000">
            <img  src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($s->filename); ?>" class="d-block w-100 " alt="...">
        </div>
         <?php $isFirst = false; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<section class="learn_shep"  style="height:700px">
    <div class="container">            
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-5"  style="margin-top:-50px">
                <div class="title">
                    <h2>Take The First Step To Learn With Us.</h2>
                    <p>"It is our goal to train and nurture a holistic child though quality education in a safe environment which stimulates growth, love for learning and leadership. Every child will be given the opportunity to develop physically, socially,emotionally and intellectually at his on her own pace."</p>
                    <a href="<?php echo e(config('website.AppPortal')); ?>/application-form" title="">Get Started</a>
                 </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">            
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-7 ml-auto p-0" >
                <div class="shep_banner_wrapper">
                    <div class=" step_single_banner">
                        <?php if(!empty($photo[0]['filename'])): ?>
                        <img class="resize2" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($photo[0]['filename']); ?>" alt="" class="img-fluid">
                        <?php endif; ?>
                        <?php if(!empty($photo[1]['filename'])): ?>
                        <img class="resize2" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($photo[1]['filename']); ?>" alt="" class="img-fluid">
                        <?php endif; ?>
                    </div>
                    <div class="step_single_banner" >
                        <?php if(!empty($photo[2]['filename'])): ?>
                        <img class="resize2" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($photo[2]['filename']); ?>" alt="" class="img-fluid">
                        <?php endif; ?>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</section><!-- End Larnign Step -->

<section class="events-area">
    <div class="container">
        <div class="row" >
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="event_title_wrapper"  style="margin-top:-100px">  
                    <div class="sub_title">
                        <h2>Our Upcoming Events</h2>
                      
                    </div><!-- ends: .section-header -->
                    <div class="envent_all_view">
                        <a href="#" title="">View All</a>
                    </div>
                </div> 
            </div>
        </div>

        <div class="row">
            <?php if(!empty( $events[0]['filename'])): ?>
            <div class="col-sm-12 events_full_box">
                <div class="events_single">
                    <div class="event_banner">
                        <a href="#"><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($events[0]['filename']); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="event_info">
                        <h3><a href="#" title=""><?php echo e($events[0]['event_name']); ?></a></h3>
                        <div class="events_time">
                            <span class="time"><i class="flaticon-clock-circular-outline"></i><?php echo e(date('g:i a',strtotime($events[0]['start_date']))); ?> - <?php echo e(date('g:i a',strtotime($events[0]['end_date']))); ?></span>
                            <span><i class="fas fa-map-marker-alt"></i><?php echo e($events[0]['location']); ?></span>
                        </div>
                        <p><?php echo e(str_limit($events[0]['event_description'], 150)); ?></p>
                        <div class="event_dete">
                            <span class="date"><?php echo e(date('d',strtotime( $events[0]['start_date'] ))); ?></span>
                            <span><?php echo e(str_limit(date('F',strtotime( $events[0]['start_date'] )), 3)); ?></span>
                        </div>
                    </div>
                </div>  
            </div>
            <?php endif; ?>
            <?php if(!empty( $events[1]['filename'])): ?>
            <div class="col-sm-12 events_full_box">
                <div class="events_single events_single_left">
                    <div class="event_info">
                        <h3><a href="#" title=""><?php echo e($events[1]['event_name']); ?></a></h3>
                        <div class="events_time">
                            <span class="time"><i class="flaticon-clock-circular-outline"></i><?php echo e(date('g:i a',strtotime($events[1]['start_date']))); ?> - <?php echo e(date('g:i a',strtotime($events[1]['end_date']))); ?></span>
                            <span><i class="fas fa-map-marker-alt"></i><?php echo e($events[1]['location']); ?></span>
                        </div>
                         <p><?php echo e(str_limit($events[1]['event_description'], 150)); ?></p>
                    </div>
                    <div class="event_banner">
                        <a href="#"><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($events[1]['filename']); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="event_dete">
                        <span class="date"><?php echo e(date('d',strtotime( $events[1]['start_date'] ))); ?></span>
                        <span><?php echo e(str_limit(date('F',strtotime( $events[1]['start_date'] )), 3)); ?></span>
                    </div>
                </div>  
            </div>
             <?php endif; ?>
             <?php if(!empty( $events[2]['filename'])): ?>
            <div class="col-sm-12 events_full_box">
                <div class="events_single" >
                    <div class="event_banner">
                        <a href="#"><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($events[2]['filename']); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="event_info">
                        <h3><a href="#" title=""><?php echo e($events[2]['event_name']); ?></a></h3>
                        <div class="events_time">
                            <span class="time"><i class="flaticon-clock-circular-outline"></i><?php echo e(date('g:i a',strtotime($events[2]['start_date']))); ?> - <?php echo e(date('g:i a',strtotime($events[2]['end_date']))); ?></span>
                            <span><i class="fas fa-map-marker-alt"></i><?php echo e($events[2]['location']); ?></span>
                        </div>
                         <p><?php echo e(str_limit($events[2]['event_description'], 150)); ?></p>
                        <div class="event_dete">
                            <span class="date"><?php echo e(date('d',strtotime( $events[2]['start_date'] ))); ?></span>
                            <span><?php echo e(str_limit(date('F',strtotime( $events[2]['start_date'] )), 3)); ?></span>
                        </div>
                    </div>
                </div>  
            </div>            
            <?php endif; ?>
        </div>
    </div>
</section><!-- ./ End Events Area section -->


<!--Gallery -->

<section class="blog " style="margin-top: -170px; margin-bottom: 100px">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="sub_title">
                    <h2>Our Gallery</h2>  
                </div><!-- ends: .section-header -->
            </div>
        </div>
       
        <div class="slick">
            <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card" style="width: 18rem; margin-left: 3px">
                <img class="resize"  style="height=50px" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($p->filename); ?>" class="card-img-top" alt="...">
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section><!-- End Gallery  -->

<section class="video_online" style="margin-top:-190px; height:480px">
	<div class="container-fluid" style="background-color: #FFE0B2">
		<div class="row">
			<div class="col-6 col-sm-6 col-md-3 col-lg-3 counter_single_wrapper">
				<div class="section count_single">
					<div class="project-count counter">1500</div>
					<span>Active students</span>
				</div>
			</div>

			<div class="col-6 col-sm-6 col-md-3 col-lg-3 counter_single_wrapper">
				<div class="section count_single">
					<div class="project-count counter">30</div>
					<span>Teachers</span>
				</div>  
			</div>  

			<div class="col-6 col-sm-6 col-md-3 col-lg-3 counter_single_wrapper">
				<div class="section count_single">
					<div class="project-count"><span class="counter">100</span><span class="count_icon">%</span></div>
					<span>Satisfaction</span>
				</div>
			</div>

			<div class="col-6 col-sm-6 col-md-3 col-lg-3 counter_single_wrapper">
				<div class="section count_single">
					<div class="project-count counter">2000</div>
					<span>Graduates</span>
				</div>  
			</div>  
		</div>
	</div>
	<div class="bg_shapes">
	</div>
</section>

<section class="blog d-none d-sm-block" >
    <div class="container" >
        <div class="row"  style="margin-top:-140px">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="sub_title">
                    <h2>Our Latest News</h2>
                </div><!-- ends: .section-header -->
            </div>
        </div>
        <div class="row">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                <div class="single_item single_item_first">
                    <div class="blog-img">
                        <a href="<?php echo e(route('blog-details',$p->slug)); ?>" title=""><img class="resize" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($p->filename); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="blog_title">  
                        <h3><a href="<?php echo e(route('blog-details',$p->slug)); ?>" title=""> <?php echo e($p->title); ?>.</a></h3> 
                        <div class="post_bloger">
                            <span><?php echo e(date('d F Y',strtotime($p->created_at))); ?> - By </span> <span class="bloger_name"> Admin</span>
                        </div>               
                    </div>   
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section><!-- End Blog  -->

<!--Mobile Blog -->

<section class="blog d-block d-sm-none" style="margin-top: -90px;">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="sub_title">
                    <h2>Our Latest News</h2>  
                </div><!-- ends: .section-header -->
            </div>
        </div>
        <div class="slick">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class=""  >
                 <div class="single_item single_item_first" >
                    <div class="blog-img">
                        <a href="<?php echo e(route('blog-details',$p->slug)); ?>" title=""><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($p->filename); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="blog_title">
                        <h3><a href="<?php echo e(route('blog-details',$p->slug)); ?>" title=""> <?php echo e($p->title); ?></a></h3> 
                        <div class="post_bloger">
                            <span><?php echo e(date('d F Y',strtotime($p->created_at))); ?> - By </span> <span class="bloger_name"> Admin</span>
                        </div>               
                    </div>   
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section><!-- End Blog  -->

<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('slick/slick/slick.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('slick/slick/slick-theme.css')); ?>" rel="stylesheet" type="text/css" />
<style>
 .resize{
    width: 500px;
    height: 300px;
    background-size: cover;
}

</style>
<?php $__env->stopPush(); ?>
<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('slick/slick/slick.min.js')); ?>" type="text/javascript"></script>
<script>
    $('.slick').slick({

  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: true,
  prevArrow: '<button type="button" class="slick-prev badge bg-secondary">Previous</button>',
  nextArrow: '<button type="button" class="slick-next badge bg-secondary">Next</button>',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
    </script>
<?php $__env->stopPush(); ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/home.blade.php ENDPATH**/ ?>