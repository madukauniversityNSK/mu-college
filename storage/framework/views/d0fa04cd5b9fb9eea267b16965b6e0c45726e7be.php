<?php $__env->startSection('title', 'Blog-Details'); ?>

<?php $__env->startSection('content'); ?>

<hr class=" d-none d-sm-block" style="margin-top: -5px">
<section class="blog_wrapper" style="margin-top:-100px">
    <div class="container">  
        <div class="row" >        
            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                <div class="blog_post">
                    <h3><?php echo e($record->title); ?></h3>
                    <div class="post_by d-flex">
                        <span>By - <a href="#" title="" class="bloger_name">Admin</a></span>
                        <span>Posted On : <?php echo e(date('d F Y',strtotime($record->created_at))); ?></span>                        
                    </div>
                    <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($record->filename); ?>" alt="" class="img-fluid">
                    <div class="postpage_content_wrapper">
                        <div class="blog_post_content">
                            <?php echo e(Illuminate\Mail\Markdown::parse($record->content)); ?>

                        </div>
                    </div>
                </div>               
            </div> <!-- End Blog Left Side-->

            <div class="col-12 col-sm-12 col-md-4 col-lg-4 blog_wrapper_right ">
                <div class="blog-right-items">
                    <div class="recent_post_wrapper widget_single">
                        <div class="items-title">
                            <h3 class="title">Recent Post</h3>
                        </div>
                        <?php $__currentLoopData = $recents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="single-post">
                            <div class="recent_img">
                                 <a href="<?php echo e(route('blog-details',$recent->slug)); ?>" title=""><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($recent->filename); ?>" alt="" class="img-fluid"></a>
                            </div>
                           
                            <div class="post_title">
                                <a href="<?php echo e(route('blog-details',$recent->slug)); ?>" title=""><?php echo e($recent->title); ?></a>
                                <div class="post-date">
                                    <span><?php echo e(date('d F Y',strtotime($recent->created_at))); ?></span>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div><!-- ./ End  Blog Right Side-->
            
        </div>
    </div> 
</section> <!-- ./ End Blog Area Wrapper-->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/blog-details.blade.php ENDPATH**/ ?>