<?php $__env->startSection('title', 'Post Edit'); ?>

<?php $__env->startSection('content'); ?>
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Post Edit</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <form action="<?php echo e(route('update-post')); ?>" method='POST' class="py-4" enctype="multipart/form-data">  
                                 <?php echo csrf_field(); ?>
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Title</label>  
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="title" value="<?php echo e(old('title', $record->title )); ?>"  required>  
                                    <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset> 
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Image</label>  
                                    <input type="file" class=" form-control form-control <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="image" value="<?php echo e(old('image')); ?>" >  
                                    <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Content</label>  
                                    <textarea name="content"  class=" summernote form-control form-control <?php if ($errors->has('content')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('content'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="summernote"  required><?php echo e(old('content', $record->content)); ?></textarea>
                                    <?php if ($errors->has('content')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('content'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                                <input type="hidden"  name="id" value="<?php echo e(old('id', $record->id)); ?>" autofocus>
                                <div class="">
                                    <button class="btn btn-lg btn-primary float-right" type="submit"  >Update</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6">
                            <img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($record->filename); ?>"  class="img-fluid" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->startPush('scripts'); ?>
    <script src="<?php echo e(asset('summernote/dist/summernote-lite.js')); ?>"></script>
    <script src="<?php echo e(asset('summernote/dist/lang/summernote-es-ES.js')); ?>"></script>
    <script>
    $('.summernote').summernote({

    });
    </script>
    
    <?php $__env->stopPush(); ?>
     
    <?php $__env->startPush('css'); ?>
      
    
    <link href="<?php echo e(asset('summernote/dist/summernote-lite.css')); ?>" rel="stylesheet"></link>
 
    <?php $__env->stopPush(); ?>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/backend/post/edit.blade.php ENDPATH**/ ?>