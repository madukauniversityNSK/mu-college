<?php $__env->startSection('title', 'Admin'); ?>

<?php $__env->startSection('content'); ?>
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left  text-dark text-capitalize">
                    <h5><i class="fa fa-plus-circle"></i> Manage Admin</h>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_tile">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Manage Admin</a>
                        </li>
                        <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Create Admin</a>
                        </li>
                    </ul>
                    <div class="tab-content py-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div>
                             <?php if(session()->has('message')): ?>
                               <div class="alert alert-success">
                                  <?php echo e(session('message')); ?>

                                </div>
                              <?php endif; ?>
                            </div>
                            <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                   
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card-box table-responsive">
                                                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Email</th>
                                                                <th>Phone</th>
                                                                <th>Role</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php if(!empty($record)): ?>
                                                        <?php $__currentLoopData = $record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr>
                                                               
                                                                <td class="text-capitalize"><?php echo e($records->full_name); ?></td>
                                                                <td><?php echo e($records->email); ?></td>
                                                                <td><?php echo e($records->phone); ?></td>
                                                                <td><?php echo e($records->user_type); ?></td>
                                                                <td>
                                                                    <div class="dropdown">
                                                                        <a class=" dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fa fa-bars text-success"></i> 
                                                                        </a>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                                            <a class="dropdown-item" href="<?php echo e(route('user.edit', $records->id)); ?>"><i class="fa fa-pencil"></i>  Edit</a>
                                                                            <a class="dropdown-item" href="<?php echo e(route('user.delete', $records->id)); ?>"><i class="fa fa-remove"></i>  Delete</a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="x_content">
                            <form action="<?php echo e(route('user.create')); ?>" method="POST" role="form" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <hr>
                            <div class="tile-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="name">Full Name</label>
                                            <input
                                                class="form-control <?php if ($errors->has('full_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('full_name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                type="text"
                                                id="name"
                                                name="full_name"
                                                value="<?php echo e(old('full_name')); ?>"
                                                required
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> <?php if ($errors->has('name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('name'); ?> <span><?php echo e($message); ?></span> <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Phone Number</label>
                                            <input
                                                class="form-control <?php if ($errors->has('phone')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('phone'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                type="number"
                                                id="phone"
                                                name="phone"
                                                value="<?php echo e(old('phone')); ?>"
                                                required
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> <span><?php echo e($message); ?></span> <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="name" >User Type</label>
                                            <select name="user_type" id="" class="form-control" required>
                                                <option value="">select user type</option>
                                                <option value="super_admin">super admin</option>
                                                <option value="admin">admin</option>
                                            </select>
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> <?php if ($errors->has('user_type')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('user_type'); ?> <span><?php echo e($message); ?></span> <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="name">Password</label>
                                            <input
                                                class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                type="password"
                                                id="password"
                                                name="password"
                                                required
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> <span><?php echo e($message); ?></span> <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="email">Email</label>
                                            <input
                                                class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                                type="email"
                                                id="email"
                                                name="email"
                                                value="<?php echo e(old('email')); ?>"
                                                required
                                            />
                                            <div class="invalid-feedback active">
                                                 <i class="fa fa-exclamation-circle fa-fw"></i> <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> <span><?php echo e($message); ?></span> <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="tile-footer">
                                    <div class="row d-print-none mt-2">
                                        <div class="col-12 text-right">
                                              <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add User</button>
                                        </div>
                                     </div>
                                </div>
                            </div>
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/backend/users/index.blade.php ENDPATH**/ ?>