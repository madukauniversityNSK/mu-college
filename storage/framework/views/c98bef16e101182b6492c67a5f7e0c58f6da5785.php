<?php $__env->startSection('title', 'Blog-List'); ?>

<?php $__env->startSection('content'); ?>

<hr class=" d-none d-sm-block" style="margin-top: -5px">
<section class="latest_news_2" id="latest_news_style_2" style="margin-top:-80px">
    <div class="container">
        <div class="row">
        <?php if($records): ?>
        <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                 <div class="single_item">
                    <div class="item_wrapper">
                        <div class="blog-img">
                            <a href="<?php echo e(route('blog-details',$record->slug)); ?>" title=""><img src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e($record->filename); ?>" alt="" class="img-fluid"></a>
                        </div>
                        <h3><a href="<?php echo e(route('blog-details',$record->slug)); ?>" title=""><?php echo e($record->title); ?></a></h3> 
                    </div>
                    <div class="blog_title">
                        <ul class="post_bloger">
                            <li><i class="fas fa-user"></i>Admin</li> 
                            <li><i class="fas fa-clock"></i><?php echo e(date('d F Y',strtotime($record->created_at))); ?></li>
                        </ul>               
                    </div> 
                </div>
            </div>                       
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php echo e($records->links()); ?>

        <?php else: ?>
        <div class="text-center">
            sorry no blog post
        </div>
        <?php endif; ?>
    </div>
</section><!-- End Our Blog -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/blog-list.blade.php ENDPATH**/ ?>