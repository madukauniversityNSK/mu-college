<?php $__env->startSection('title', 'Settings'); ?>

<?php $__env->startSection('content'); ?>
      
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left text-dark">
                    <h5><i class="fa fa-plus-circle"></i> Manage Settings</h5>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5   form-group row pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                             <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                       </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php if(session()->has('message')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session('message')); ?>

                    </div>
                    <?php endif; ?>
                    <form action="<?php echo e(route('admin.settings.update')); ?>" method='POST' class="py-4" enctype="multipart/form-data">  
                       <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Name of School *</label>  
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="school_name" value="<?php echo e(config('settings.school_name')); ?>"  required>  
                                    <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div> 
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Current Session *</label>  
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('date'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id=""  value="<?php echo e(config('settings.current_session')); ?>" name="current_session" required>  
                                     <?php if ($errors->has('date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('date'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Phone Number</label>  
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('time')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('time'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="phone_number" value="<?php echo e(config('settings.phone_number')); ?>" >  
                                    <?php if ($errors->has('time')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('time'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div> 
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">School Email</label>  
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('venue')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('venue'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="email_address" value="<?php echo e(config('settings.email_address')); ?>" >  
                                    <?php if ($errors->has('venue')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('venue'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">School Address</label> 
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="school_address" value="<?php echo e(config('settings.school_address')); ?>"  >  
                                    <?php if ($errors->has('image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('image'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div>
                            <div class="col-sm-6">
                               <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">School Logo</label>  
                                    <input type="file" class=" form-control form-control <?php if ($errors->has('logo')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('logo'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="logo" value="" >  
                                    <?php if ($errors->has('content')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('content'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">About Us</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="about_us"><?php echo e(config('settings.about_us')); ?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Mission</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="mission"><?php echo e(config('settings.mission')); ?></textarea>
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Vision</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="vision"><?php echo e(config('settings.vision')); ?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Motto</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="motto"><?php echo e(config('settings.motto')); ?></textarea>
                                </div>
                            </div>     
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Facebook Url</label> 
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('facebook_url')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('facebook_url'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="facebook_url" value="<?php echo e(config('settings.facebook_url')); ?>"  >  
                                    <?php if ($errors->has('facebook_url')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('facebook_url'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div>
                            <div class="col-sm-4">
                               <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Instagram Url</label>  
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('instagram_url')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('instagram_url'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="instagram_url" value="<?php echo e(config('settings.instagram_url')); ?>" > 
                                    <?php if ($errors->has('instagram_url')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('instagram_url'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div>
                            <div class="col-sm-4">
                               <fieldset class="form-group mb-3">   
                                    <label for="exampleFormControlInput1" class="form-label">Twitter Url</label>  
                                    <input type="text" class=" form-control form-control <?php if ($errors->has('twitter_url')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('twitter_url'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" id="exampleFormControlInput1"  name="twitter_url" value="<?php echo e(config('settings.twitter_url')); ?>" > 
                                    <?php if ($errors->has('twitter_url')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('twitter_url'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </fieldset>
                            </div>
                        </div>
                        <div class="">
                            <button class="btn btn-lg btn-primary" type="submit"  >Update</button>
                        </div>
                   </form>
                   <div class="text-center bg-secondary">
                        <img height="150px" class="" src="https://raadaa-bucket.s3.eu-west-2.amazonaws.com/<?php echo e(config('settings.logo')); ?>"  class="img-fluid" alt="image">
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <?php $__env->startPush('scripts'); ?>
    <script src="<?php echo e(asset('summernote/assets/summernote-lite.js')); ?>"></script>
    <script src="<?php echo e(asset('summernote/assets/lang/summernote-es-ES.js')); ?>"></script>
    <script>
    $('.summernote').summernote({

    });
    </script>
    
    <?php $__env->stopPush(); ?>
     
    <?php $__env->startPush('css'); ?>
      
    
    <link href="<?php echo e(asset('summernote/assets/summernote-lite.css')); ?>" rel="stylesheet"></link>
 
    <?php $__env->stopPush(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ixora\resources\views/backend/settings/index.blade.php ENDPATH**/ ?>