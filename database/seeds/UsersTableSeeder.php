<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('users')->insert([
            'full_name' => 'Super Admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('admin-secret'),
            'user_type' => 'super_admin',
            'phone'  => '08161155633'
        ]);
    }
}
