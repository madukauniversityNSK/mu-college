<?php
namespace Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();

        $data = [
            [
                'key'                       =>  'current_session',
                'value'                     =>  '2018-2019',
            ],
            [
                'key'                       =>  'school_name',
                'value'                     =>  'Prince School',
            ],
            [
                'key'                       =>  'email_address',
                'value'                     =>  'admin@admin.com',
            ],
            [
                'key'                       =>  'school_address',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'logo',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'About_us',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'vision',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'mission',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'phone_number',
                'value'                     =>  '08161155633',
            ],
            [
                'key'                       =>  'motto',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'facebook url',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'instagram url',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'twitter url',
                'value'                     =>  '',
            ]
        ];

        DB::table('settings')->insert($data);
        //
    }
}
