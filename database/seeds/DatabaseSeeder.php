<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'key'                       =>  'current_session',
                'value'                     =>  '2018-2019',
            ],
            [
                'key'                       =>  'school_name',
                'value'                     =>  'Prince School',
            ],
            [
                'key'                       =>  'email_address',
                'value'                     =>  'admin@admin.com',
            ],
            [
                'key'                       =>  'school_address',
                'value'                     =>  'testing',
            ],
            [
                'key'                       =>  'logo',
                'value'                     =>  'testing',
            ],
            [
                'key'                       =>  'about_us',
                'value'                     =>  'testing',
            ],
            [
                'key'                       =>  'vision',
                'value'                     =>  'testing',
            ],
            [
                'key'                       =>  'mission',
                'value'                     =>  'testing',
            ],
            [
                'key'                       =>  'phone_number',
                'value'                     =>  '08161155633',
            ],
            [
                'key'                       =>  'motto',
                'value'                     =>  'testing',
            ],
            [
                'key'                       =>  'facebook_url',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'instagram_url',
                'value'                     =>  '',
            ],
            [
                'key'                       =>  'twitter_url',
                'value'                     =>  '',
            ]
        ];
        DB::table('settings')->insert($data);

        $this->call([UsersTableSeeder::class,]);
    }
}
