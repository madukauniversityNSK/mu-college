<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('info');
            $table->string('url');
            $table->longText('address');
            $table->string('email');
            $table->string('phone');
            $table->string('logo1');
            $table->string('logo2');
            $table->string('logo3');
            $table->timestamps();
        });
        DB::table('sites')->insert([ //,

            'name' => "Benue1",
            'info' => "The site of God's",
            'url' => "https://google.com",
            'address' => "56, bsc close onodagu",
            'email' =>"info@example.com",
            'phone' => '08012345678',
            'logo1' => 'logo1',
            'logo2' => 'logo2',
            'logo3' => 'logo3',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
