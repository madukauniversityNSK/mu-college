<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('content');
            $table->uuid('uuid')->nullable();
            $table->uuid('user_id');
            $table->string('title')->nullable();
            $table->boolean('comment')->default(1);
            $table->boolean('stop_comment')->default(0);
            $table->string('slug')->default('1234');
            $table->string('filename')->nullable();
            $table->string('mime')->nullable();
            $table->boolean('is_page')->default(0);
            $table->timestamps();
        });

        DB::table('posts')->insert([ //,

            'content' => "Games",
            'uuid' => "12345",
            'user_id' =>'12345',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
