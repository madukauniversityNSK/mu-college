<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('about_text');
            $table->text('philosophy');
            $table->text('motto');
            $table->text('vision');
            $table->text('mission');
            $table->timestamps();
        });
        DB::table('about')->insert([ //,

            'about_text' => "random tetx",
            'philosophy' => "Random",
            'motto' =>'random',
            'vision' => 'random',
            'mission' => 'random',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about');
    }
}
